import React, { Component } from "react";
import moment from "moment";
import { connect } from "react-redux";
import { addCategory } from "../../../redux/actions/categoryActions";
import { addFinEvent } from "../../../redux/actions/finEventActions";
import {
  selectDateChange,
  //MODAL NO MORE IN USE
  //modalManage,
  updateDisplayBalance,
  manageIsFormState,
  getUser,
} from "../../../redux/actions/appActions";
import AddButton from "../../AddButton/AddButton";
import DailyViewWrapper from "../views/DailyView/DailyViewWrapper";
import { getDateRange } from "../../../services/utilityService/utilityService";
import classes from "./Daily.module.css";

class Daily extends Component {
  state = {
    id: "",
  };
  redirectToCreateNewHandler = (event, headerDate) => {
    const token = localStorage.getItem("AccessToken");
    let createDate;
    if (headerDate) {
      createDate = new Date(headerDate);
    } else {
      createDate = new Date();
    }
    this.props.getUser({ token: token });
    this.props.history.push(
      `/financialEvent/new/${moment(createDate).format("YYYY-MM-DD")}`
    );
  };
  redirectToEditFinEventHandler = (event, data) => {
    //MODAL NO MORE IN USE
    //this.props.modalManage(true, "EditFinancialEvent", data);
    this.props.history.push(`/financialEvent/edit/${data}`);
  };
  sortHandle = (event, desc) => {
    event.preventDefault();
    if (desc) {
      this.props.history.push(
        `/daily/${moment(this.props.match.params.selectDate).format(
          "YYYY-MM-DD"
        )}/desc`
      );
    } else {
      this.props.history.push(
        `/daily/${moment(this.props.match.params.selectDate).format(
          "YYYY-MM-DD"
        )}`
      );
    }
  };
  getFinancialEvents = (selectDate, token) => {
    const dateRange = getDateRange("daily", selectDate);
    this.props.addFinEvent({
      start: dateRange.start,
      end: dateRange.end,
      token: token,
    });
  };

  componentDidUpdate(prevProps, prevState) {
    if (prevProps.finEvent !== this.props.finEvent) {
      if (typeof this.props.finEvent !== "undefined")
        this.props.displayBalance("daily", this.props.finEvent);
    }
    if (prevProps.selectDate.toString() !== this.props.selectDate.toString()) {
      if (
        !moment(
          this.props.match.params.selectDate,
          moment.ISO_8601,
          true
        ).isValid()
      ) {
        this.props.history.push(`/daily/${moment().format("YYYY-MM-DD")}`);
        this.props.selectDateChange(new Date(), "month");
      }

      //call DB
      const token = localStorage.getItem("AccessToken");
      this.getFinancialEvents(this.props.selectDate, token);
    }
    if (
      prevProps.match.params.selectDate !== this.props.match.params.selectDate
    ) {
      this.props.selectDateChange(
        new Date(this.props.match.params.selectDate),
        "month"
      );
    }
  }
  componentDidMount = () => {
    const token = localStorage.getItem("AccessToken");

    const id =
      this.props.location.search.indexOf("day=") > 0
        ? this.props.location.search.slice(
            this.props.location.search.indexOf("day=") + 4
          )
        : "";
    if (id !== this.state.id) {
      if (id !== "") {
        this.setState({ id: id });
      }
    }

    this.props.selectDateChange(
      new Date(moment(this.props.match.params.selectDate)),
      "month"
    );
    this.getFinancialEvents(
      new Date(moment(this.props.match.params.selectDate)),
      token
    );
    this.props.addCategory({ token: token });
    this.props.manageIsFormState(false);
  };
  render() {
    const element =
      this.state.id !== "" ? document.getElementById(this.state.id) : null;
    if (element !== null) {
      element.scrollIntoView({ behavior: "smooth", block: "center" });
    }
    return (
      <div className={classes.DailyContent}>
        <DailyViewWrapper
          onRowClick={this.redirectToEditFinEventHandler}
          onGroupClick={this.redirectToCreateNewHandler}
          category={this.props.category}
          finEvent={this.props.finEvent && this.props.finEvent.toJS()}
          order={this.props.match.params.order}
          selectDate={this.props.match.params.selectDate}
          onSortClick={this.sortHandle}
          classes={classes}
        ></DailyViewWrapper>

        <AddButton onclick={this.redirectToCreateNewHandler} />
      </div>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    selectDate: state.appState.get("selectDate"),
    dateType: state.appState.get("dateType"),
    finEvent: state.financialEventState.get("finEvent"),
    category: state.categoryState.get("category"),
  };
};
const mapDispatchToProps = {
  selectDateChange: selectDateChange,
  //MODAL NO MORE IN USE
  //modalManage: modalManage,
  addFinEvent: addFinEvent,
  addCategory: addCategory,
  displayBalance: updateDisplayBalance,
  manageIsFormState: manageIsFormState,
  getUser: getUser,
};

export default connect(mapStateToProps, mapDispatchToProps)(Daily);
