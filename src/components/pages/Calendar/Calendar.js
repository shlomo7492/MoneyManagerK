/* eslint-disable no-loop-func */
import React, { Component } from "react";
import moment from "moment";
import { connect } from "react-redux";
import { addFinEvent } from "../../../redux/actions/finEventActions";
import {
  selectDateChange,
  updateDisplayBalance,
  manageIsFormState,
} from "../../../redux/actions/appActions";
import {
  getDateRange,
  getCalendarSummaryData,
  getFormattedDataCalendar,
} from "../../../services/utilityService/utilityService";
import CalendarView from "../views/CalendarView/CalendarView";
import AddButton from "../../AddButton/AddButton";

class Calendar extends Component {
  redirectToCreateNewHandler = (event, date) => {
    const dateToStr = date
      ? moment(date).format("YYYY-MM-DD")
      : moment().format("YYYY-MM-DD");
    this.props.history.push(`/financialEvent/new/${dateToStr}`);
  };
  getFinancialEvents = (selectDate, token) => {
    const dateRange = getDateRange("calendar", selectDate);
    this.props.addFinEvent({
      start: dateRange.start,
      end: dateRange.end,
      token: token,
    });
  };

  redirectToDaily = (eventDate, id) => {
    this.props.history.push(`/daily/${eventDate}?day=${id}`);
  };
  componentDidUpdate = (prevProps, prevState) => {
    if (prevProps.finEvent !== this.props.finEvent) {
      if (typeof this.props.finEvent !== "undefined")
        this.props.displayBalance(
          "calendar",
          getCalendarSummaryData(this.props.selectDate, this.props.finEvent)
        );
    }
    if (prevProps.selectDate.toString() !== this.props.selectDate.toString()) {
      if (
        !moment(
          this.props.match.params.selectDate,
          moment.ISO_8601,
          true
        ).isValid()
      ) {
        this.props.history.push(`/calendar/${moment().format("YYYY-MM-DD")}`);
        this.props.selectDateChange(new Date(), "month");
      } else {
        this.props.history.push(
          `/calendar/${moment(this.props.selectDate).format("YYYY-MM-DD")}`
        );
      }
      //call DB
      const token = localStorage.getItem("AccessToken");
      this.getFinancialEvents(this.props.selectDate, token);
    }
  };
  componentDidMount = () => {
    const token = localStorage.getItem("AccessToken");
    this.props.selectDateChange(
      new Date(moment(this.props.match.params.selectDate)),
      "month"
    );
    this.getFinancialEvents(
      new Date(moment(this.props.match.params.selectDate)),
      token
    );
    this.props.manageIsFormState(false);
  };
  render() {
    return (
      <>
        <CalendarView
          finEvent={
            this.props.finEvent &&
            getFormattedDataCalendar(this.props.finEvent, this.props.selectDate)
          }
          clickedCalendarDay={this.redirectToDaily}
          clickedCalendarDayAddFinEvent={this.redirectToCreateNewHandler}
          selectDate={this.props.selectDate}
        />
        <AddButton onclick={this.redirectToCreateNewHandler} />
      </>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    finEvent: state.financialEventState.get("finEvent"),
    selectDate: state.appState.get("selectDate"),
    dateType: state.appState.get("dateType"),
  };
};
const mapDispatchToProps = {
  selectDateChange: selectDateChange,
  addFinEvent: addFinEvent,
  displayBalance: updateDisplayBalance,
  manageIsFormState: manageIsFormState,
};

export default connect(mapStateToProps, mapDispatchToProps)(Calendar);
