import React, { Component } from "react";
import moment from "moment";
import { connect } from "react-redux";
import { Card } from "react-bootstrap";
import { Parser } from "json2csv";
import fileDownload from "js-file-download";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faFileExcel } from "@fortawesome/free-regular-svg-icons";
import {
  selectDateChange,
  updateDisplayBalance,
  manageIsFormState,
} from "../../../redux/actions/appActions";
import {
  addFinEvent,
  addPrevMonthSummary,
} from "../../../redux/actions/finEventActions";

import {
  getDateRange,
  compareExpensesIncreaseWithPrevMonth,
} from "../../../services/utilityService/utilityService";
import classes from "./Total.module.css";
class Total extends Component {
  getFinancialEvents = (selectDate, token) => {
    const dateRange = getDateRange("daily", selectDate);
    this.props.addFinEvent({
      start: dateRange.start,
      end: dateRange.end,
      token: token,
    });
  };
  //TODO: method to get prevMonth expenses
  getPrevMonthExpenses = (selectDate, token) => {
    const prevMonthSelectDate = moment(selectDate)
      .subtract(1, "month")
      .format("YYYY-MM-DD");
    const dateRange = getDateRange("daily", prevMonthSelectDate);
    this.props.addPrevMonthSummary({
      start: dateRange.start,
      end: dateRange.end,
      type: "expense",
      token: token,
    });
  };

  finEventToCSVConverter = (finEvent) => {
    const fields = ["date", "description", "amount", "type"];
    const csvParser = new Parser({ fields });
    const csvData = csvParser.parse(finEvent);
    return csvData;
  };
  downloadCSVHandler = () => {
    const finEvent = this.props.finEvent.toJS();
    fileDownload(
      this.finEventToCSVConverter(finEvent),
      "financialEventsData.csv"
    );
  };
  componentDidUpdate(prevProps, prevState) {
    const token = localStorage.getItem("AccessToken");

    if (prevProps.finEvent !== this.props.finEvent) {
      if (typeof this.props.finEvent !== "undefined")
        this.props.displayBalance("daily", this.props.finEvent);
    }

    if (
      prevProps.match.params.selectDate.toString() !==
      this.props.match.params.selectDate.toString()
    ) {
      this.getFinancialEvents(this.props.match.params.selectDate, token);
      this.getPrevMonthExpenses(this.props.selectDate, token);
    }
    if (prevProps.selectDate.toString() !== this.props.selectDate.toString()) {
      if (
        !moment(
          this.props.match.params.selectDate,
          moment.ISO_8601,
          true
        ).isValid()
      ) {
        this.props.history.push(`/total/${moment().format("YYYY-MM-DD")}`);
        this.props.selectDateChange(new Date(), "month");
      } else {
        this.props.history.push(
          `/total/${moment(this.props.selectDate).format("YYYY-MM-DD")}`
        );
      }
    }
  }
  componentDidMount = () => {
    const token = localStorage.getItem("AccessToken");
    this.props.selectDateChange(
      new Date(moment(this.props.match.params.selectDate)),
      "month"
    );
    this.getFinancialEvents(this.props.match.params.selectDate, token);
    this.getPrevMonthExpenses(this.props.selectDate, token);
    this.props.manageIsFormState(false);
  };
  render() {
    return (
      <Card bsPrefix={classes.Card}>
        <Card.Header bsPrefix={classes.CardHeader}>
          <div className={classes.Header}>
            <div className="p-3">
              <h4>
                {moment(this.props.selectDate)
                  .startOf("month")
                  .format("DD.MM.YY")}
                ~
                {moment(this.props.selectDate)
                  .endOf("month")
                  .format("DD.MM.YY")}
              </h4>
            </div>
            <div className={"d-flex flex-column p-2 " + classes.CSVText}>
              <span
                className={classes.ExcelIcon}
                onClick={this.downloadCSVHandler}
              >
                <FontAwesomeIcon icon={faFileExcel} />
              </span>
              <span className={classes.iconText}>CSV</span>
            </div>
          </div>
        </Card.Header>
        <Card.Body>
          <span className="text-secondary">
            Compare expenses (Last month):{" "}
          </span>
          {compareExpensesIncreaseWithPrevMonth(
            this.props.prevMonthExpenses,
            this.props.finEvent
          ) + "%"}
        </Card.Body>
      </Card>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    selectDate: state.appState.get("selectDate"),
    dateType: state.appState.get("dateType"),
    finEvent: state.financialEventState.get("finEvent"),
    prevMonthExpenses: state.financialEventState.get("prevMonthExpenses"),
    category: state.categoryState.get("category"),
  };
};
const mapDispatchToProps = {
  selectDateChange: selectDateChange,
  addFinEvent: addFinEvent,
  displayBalance: updateDisplayBalance,
  manageIsFormState: manageIsFormState,
  addPrevMonthSummary: addPrevMonthSummary,
};

export default connect(mapStateToProps, mapDispatchToProps)(Total);
