import React, { Component } from "react";
import { Card, Button } from "react-bootstrap";
import UpdateUserPasswordForm from "../../../containers/UserForm/UpdateUserPassword";
//import CollapsibleArrea from "../../../containers/CollapsibleComponent/Collapsible";
import { connect } from "react-redux";
import {
  getUser,
  updateUser,
  manageIsFormState,
} from "../../../redux/actions/appActions";

import classes from "./User.module.css";

class User extends Component {
  state = { currentUser: "", isForm: false };

  submitHandler = (passwords) => {
    const token = localStorage.getItem("AccessToken");
    const data = {
      password: passwords.password,
      newPassword: passwords.newPassword,
      email: this.state.currentUser.userEmail,
      userId: this.state.currentUser.userId,
      token: token,
    };
    this.props.updateUser(data);
    this.setState({ isForm: false });
  };
  changePasswordClickHandler = () => {
    this.setState({ isForm: true });
  };
  cancelFormClickHandler = () => {
    this.setState({ isForm: false });
  };
  componentDidUpdate = (prevState, prevProps) => {
    if (prevProps.user !== this.props.user) {
      console.log(prevProps.user, this.props.user);
      const user = this.props.user;
      const currentUser = user ? user : { userName: "", userEmail: "" };
      this.props.manageIsFormState(true);
      if (this.state.currentUser !== this.props.user)
        this.setState({ currentUser: currentUser });
    }
  };
  componentDidMount = () => {
    const token = localStorage.getItem("AccessToken");
    if (token) {
      this.props.getUser({ token: token });
    }
  };
  render() {
    return (
      <Card bsPrefix={classes.Card}>
        <Card.Header bsPrefix={classes.CardHeader}>
          <h2>Profile</h2>
        </Card.Header>
        <div className={classes.CardBody}>
          {!this.state.isForm ? (
            <>
              <div>Name: {this.state.currentUser.userName}</div>
              <div>Email: {this.state.currentUser.userEmail}</div>
              <div>
                <hr />
              </div>

              <Button
                variant="outline-primary"
                onClick={this.changePasswordClickHandler}
              >
                Change Password
              </Button>
            </>
          ) : (
            <UpdateUserPasswordForm
              onSubmit={this.submitHandler}
              onCancel={this.cancelFormClickHandler}
            />
          )}
        </div>
      </Card>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    user: state.appState.get("user"),
  };
};
const mapDispatchToProps = {
  getUser: getUser,
  updateUser: updateUser,
  manageIsFormState: manageIsFormState,
};
export default connect(mapStateToProps, mapDispatchToProps)(User);
