import React, { Component } from "react";
import { connect } from "react-redux";
import moment from "moment";
import MonthlyView from "../views/MonthlyView/MonthlyView";
import {
  selectDateChange,
  updateDisplayBalance,
  manageIsFormState,
} from "../../../redux/actions/appActions";
import { addFinEvent } from "../../../redux/actions/finEventActions";
import {
  getDateRange,
  getFormattedDataMonthly,
} from "../../../services/utilityService/utilityService";
import AddButton from "../../AddButton/AddButton";

class Monthly extends Component {
  redirectToCreateNewHandler = (event) => {
    this.props.history.push(
      `/financialEvent/new/${this.props.match.params.selectDate}`
    );
  };
  getFinancialEvents = (selectDate, token) => {
    const dateRange = getDateRange("monthly", selectDate);
    this.props.addFinEvent({
      start: dateRange.start,
      end: dateRange.end,
      token: token,
    });
  };
  onClickRowHandler = (date) => {
    const queryDate = moment(date).format("YYYY-MM-DD");
    this.props.history.push(`/daily/${queryDate}`);
  };
  componentDidUpdate = (prevProps, prevState) => {
    if (prevProps.finEvent !== this.props.finEvent) {
      if (typeof this.props.finEvent !== "undefined")
        this.props.displayBalance("monthly", this.props.finEvent);
    }
    if (prevProps.selectDate.toString() !== this.props.selectDate.toString()) {
      if (
        !moment(
          new Date(this.props.match.params.selectDate).toISOString(),
          moment.ISO_8601,
          true
        ).isValid()
      ) {
        this.props.history.push(`/monthly/${moment().format("YYYY-MM-DD")}`);
        this.props.selectDateChange(new Date(), "year");
      } else {
        this.props.history.push(
          `/monthly/${moment(this.props.selectDate).format("YYYY-MM-DD")}`
        );
      }

      //call DB
      const token = localStorage.getItem("AccessToken");
      this.getFinancialEvents(this.props.selectDate, token);
    }
  };
  componentDidMount = () => {
    const token = localStorage.getItem("AccessToken");

    this.props.selectDateChange(
      new Date(moment(this.props.match.params.selectDate)),
      "year"
    );
    this.getFinancialEvents(
      new Date(moment(this.props.match.params.selectDate)),
      token
    );
    this.props.manageIsFormState(false);
  };
  render() {
    return (
      <>
        <MonthlyView
          finEvent={
            this.props.finEvent &&
            getFormattedDataMonthly(this.props.finEvent, this.props.selectDate)
          }
          onclick={this.onClickRowHandler}
        />
        <AddButton onclick={this.redirectToCreateNewHandler} />
      </>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    finEvent: state.financialEventState.get("finEvent"),
    selectDate: state.appState.get("selectDate"),
    dateType: state.appState.get("dateType"),
  };
};
const mapDispatchToProps = {
  selectDateChange: selectDateChange,
  addFinEvent: addFinEvent,
  displayBalance: updateDisplayBalance,
  manageIsFormState: manageIsFormState,
};

export default connect(mapStateToProps, mapDispatchToProps)(Monthly);
