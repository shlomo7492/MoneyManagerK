/* eslint-disable no-loop-func */
import React, { Component } from "react";
import moment from "moment";
import { connect } from "react-redux";
import {
  selectDateChange,
  updateDisplayBalance,
  manageIsFormState,
} from "../../../redux/actions/appActions";
import { addFinEvent } from "../../../redux/actions/finEventActions";
import {
  getDateRange,
  getFormattedDataWeekly,
} from "../../../services/utilityService/utilityService";
import WeeklyView from "../views/WeeklyView/WeeklyView";
import AddButton from "../../AddButton/AddButton";

class Weekly extends Component {
  redirectToCreateNewHandler = (event) => {
    this.props.history.push(
      `/financialEvent/new/${this.props.match.params.selectDate}`
    );
  };
  getFinancialEvents = (selectDate, token) => {
    const dateRange = getDateRange("weekly", selectDate);
    this.props.addFinEvent({
      start: dateRange.start,
      end: dateRange.end,
      token: token,
    });
  };

  clickOnRowHandler = (date) => {
    let queryDate = moment(date).format("YYYY-MM-DD");
    if (moment(date).format("M") > moment(this.props.selectDate).format("M")) {
      queryDate = moment(this.props.selectDate)
        .endOf("month")
        .format("YYYY-MM-DD");
    }
    const finEvents = this.props.finEvent.toJS();
    finEvents.sort((a, b) => (a.date > b.date ? 1 : b.date > a.date ? -1 : 0));

    for (let i = finEvents.length - 1; i >= 0; i--) {
      const tempDate = moment(finEvents[i].date).format("YYYY-MM-DD");
      if (tempDate <= queryDate) {
        queryDate = tempDate;
        break;
      }
    }
    this.props.history.push(
      `/daily/${queryDate}?day=${moment(queryDate).format("DD")}`
    );
  };
  sortHandle = (event, desc) => {
    event.preventDefault();
    if (desc) {
      this.props.history.push(
        `/weekly/${moment(this.props.match.params.selectDate).format(
          "YYYY-MM-DD"
        )}/desc`
      );
    } else {
      this.props.history.push(
        `/weekly/${moment(this.props.match.params.selectDate).format(
          "YYYY-MM-DD"
        )}`
      );
    }
  };
  componentDidUpdate = (prevProps, prevState) => {
    if (prevProps.finEvent !== this.props.finEvent) {
      if (typeof this.props.finEvent !== "undefined")
        this.props.displayBalance("weekly", this.props.finEvent);
    }
    if (prevProps.selectDate.toString() !== this.props.selectDate.toString()) {
      if (
        !moment(
          new Date(this.props.match.params.selectDate).toISOString(),
          moment.ISO_8601,
          true
        ).isValid()
      ) {
        this.props.history.push(`/weekly/${moment().format("YYYY-MM-DD")}`);
        this.props.selectDateChange(new Date(), "month");
      } else {
        this.props.history.push(
          `/weekly/${moment(this.props.selectDate).format("YYYY-MM-DD")}`
        );
      }
      //call DB
      const token = localStorage.getItem("AccessToken");
      this.getFinancialEvents(this.props.selectDate, token);
    }
  };

  componentDidMount = () => {
    const token = localStorage.getItem("AccessToken");
    this.props.selectDateChange(
      new Date(moment(this.props.match.params.selectDate)),
      "month"
    );
    this.getFinancialEvents(
      new Date(moment(this.props.match.params.selectDate)),
      token
    );
    this.props.manageIsFormState(false);
  };
  render() {
    return (
      <>
        <WeeklyView
          finEvent={
            this.props.finEvent &&
            getFormattedDataWeekly(
              this.props.finEvent,
              this.props.selectDate,
              this.props.match.params.order
            )
          }
          order={this.props.match.params.order}
          onClickWeekRow={this.clickOnRowHandler}
          onSortClick={this.sortHandle}
        />
        <AddButton onclick={this.redirectToCreateNewHandler} />
      </>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    finEvent: state.financialEventState.get("finEvent"),
    selectDate: state.appState.get("selectDate"),
    dateType: state.appState.get("dateType"),
  };
};
const mapDispatchToProps = {
  selectDateChange: selectDateChange,
  addFinEvent: addFinEvent,
  displayBalance: updateDisplayBalance,
  manageIsFormState: manageIsFormState,
};

export default connect(mapStateToProps, mapDispatchToProps)(Weekly);
