import React from "react";
import moment from "moment";
import { Badge } from "react-bootstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPlus } from "@fortawesome/free-solid-svg-icons";
import classes from "./CalendarView.module.css";
const calendarView = (props) => {
  let key = 0;
  return (
    <div className={classes.CalendarViewWrapper}>
      <ul className={classes.CalendarView}>
        {props.finEvent &&
          props.finEvent.map((finE) => {
            const amountSign =
              finE.incomeSummary - finE.expensesSummary <= 0 ? "" : "+";
            const dayLabel =
              finE.dayLabel.length > 1 ? finE.dayLabel : "0" + finE.dayLabel;
            const dayNumberBadge = finE.thisMonth
              ? new Date(finE.date).getTime() ===
                new Date(moment().format("YYYY-MM-DD 00:00:00")).getTime()
                ? "w-25 text-danger text-center px-0"
                : "w-25  text-primary text-center px-0"
              : "w-25 text-secondary text-center px-0";
            const calendarDayStyle = finE.thisMonth
              ? "rounded"
              : classes.CalendarDayGrey + " rounded";
            return (
              <li key={key++} className={calendarDayStyle}>
                <div>
                  <div
                    className="w-100"
                    onClick={() =>
                      props.clickedCalendarDay(
                        moment(finE.date).format("YYYY-MM-DD"),
                        dayLabel
                      )
                    }
                  >
                    <div className="text-left">
                      <h5>
                        <Badge className={dayNumberBadge}>
                          {finE.dayLabel}
                        </Badge>
                      </h5>
                    </div>
                    <div>
                      <div
                        className={
                          finE.incomeSummary - finE.expensesSummary < 0
                            ? "text-center text-danger mt-2 mb-3 p-1"
                            : finE.incomeSummary - finE.expensesSummary >= 0
                            ? "text-center text-primary mt-2 mb-3 p-1"
                            : "invisible"
                        }
                      >
                        {amountSign +
                          (finE.incomeSummary - finE.expensesSummary).toFixed(
                            2
                          )}
                      </div>
                    </div>
                  </div>
                  <div
                    className={
                      "text-center p-1 " + classes.CalendarDayAddButton
                    }
                    onClick={(event, date) =>
                      props.clickedCalendarDayAddFinEvent(event, finE.date)
                    }
                  >
                    <FontAwesomeIcon icon={faPlus} /> Add New
                  </div>
                </div>
              </li>
            );
          })}
      </ul>
    </div>
  );
};
export default calendarView;
