import React from "react";
import { Card, Badge, Row, Col } from "react-bootstrap";
import classes from "./DailyView.module.css";

const dailyView = props => {
  //the header should have the day in a badge, also month and year,
  //and weekday on left, then there will be  total income and total expenses for that day

  const currentTime = new Date();
  const hour = currentTime.getHours();
  const minutes = currentTime.getMinutes();
  let seconds = new Date("2020-01-19 21:43:00").getSeconds();
  seconds = seconds <= 9 ? "0" + seconds : seconds;

  return (
    <Card id={props.day} bsPrefix={classes.Card}>
      <Card.Header
        onClick={(event, date) =>
          props.onclick(
            event,
            `${props.monthYear}-${props.day} ${hour}:${minutes}:${seconds}`
          )
        }
        bsPrefix={" "}
        className={classes.CardHeader}
      >
        <div>
          <Row
            style={{
              margin: "0px",
              padding: "5px 0px",
              height: "70px"
            }}
          >
            <Col xs={5} md={4} className="p-0">
              <div className={classes.HeaderDateContainer}>
                <div
                  className={
                    "text-dark font-weight-bold text-center " +
                    classes.DayHeaderInDailyCard
                  }
                >
                  {props.day}
                </div>
                <div className={classes.MonthYear + " w-50"}>
                  {props.monthYear}
                  <br />
                  <Badge
                    className={
                      props.weekDay === "Sunday"
                        ? "bg-success text-white"
                        : props.weekDay === "Saturday"
                        ? "bg-primary text-white"
                        : "bg-secondary text-white"
                    }
                  >
                    {props.weekDay}
                  </Badge>{" "}
                </div>
              </div>
            </Col>
            <Col xs={0} md={1} className={classes.HideShow}></Col>
            <Col xs={7} md={7}>
              <div className={classes.HideShow}>&nbsp;</div>
              <Row>
                <Col xs={6} className={classes.Amount}>
                  <div>Income:&nbsp;</div>
                  <div className="text-primary ">
                    {props.incomeSummary.toFixed(2)}
                  </div>
                </Col>
                <Col xs={6} className={classes.Amount}>
                  <div>Expense:&nbsp;</div>
                  <div className="text-danger">
                    {props.expenseSummary.toFixed(2)}
                  </div>
                </Col>
              </Row>
              <div className={classes.HideShow}>&nbsp;</div>
            </Col>
          </Row>
        </div>
      </Card.Header>

      <Card.Body>{props.children}</Card.Body>
    </Card>
  );
};
export default dailyView;
