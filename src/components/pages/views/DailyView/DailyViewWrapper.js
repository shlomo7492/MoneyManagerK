import React from "react";
import DailyView from "./DailyView";
import { Row, Col } from "react-bootstrap";
import moment from "moment";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faSortAlphaDown,
  faSortAlphaDownAlt,
} from "@fortawesome/free-solid-svg-icons";
import classes from "./DailyView.module.css";
const dailyViewWrapper = (props) => {
  const getOrderedAndFormatedData = (tempState, order) => {
    const viewResult = {};
    if (typeof tempState !== "undefined") {
      for (let i = tempState.length - 1; i >= 0; i--) {
        const date = moment(tempState[i].date);
        const day = date.format("DD");
        if (typeof viewResult[day] === "undefined") {
          viewResult[day] = {
            dayOfTheMonth: day,
            monthYear: date.format("YYYY-MM"),
            weekDay: date.format("dddd"),
            listOfFinEvents: [tempState[i]],
          };
          if (tempState[i].type === "income") {
            viewResult[day].incomeSummary = tempState[i].amount;
            viewResult[day].expenseSummary = 0;
          } else {
            viewResult[day].incomeSummary = 0;
            viewResult[day].expenseSummary = tempState[i].amount;
          }
        } else {
          if (tempState[i].type === "income") {
            viewResult[day].incomeSummary += tempState[i].amount;
          } else {
            viewResult[day].expenseSummary += tempState[i].amount;
          }
          viewResult[day].listOfFinEvents.push(tempState[i]);
        }
      }

      const viewArr = [];
      for (let key in viewResult) {
        viewArr.push(viewResult[key]);
      }
      return order === "desc"
        ? viewArr.sort((a, b) => (a.dayOfTheMonth > b.dayOfTheMonth ? -1 : 1))
        : viewArr.sort((a, b) => (a.dayOfTheMonth < b.dayOfTheMonth ? -1 : 1));
    }
    return [];
  };

  const orderIconsStyleDesc =
    props.order === "desc"
      ? classes.OrderIcon + " " + classes.SelectedOrderIcon
      : classes.OrderIcon;
  const orderIconsStyleAsc =
    props.order === "desc"
      ? classes.OrderIcon
      : classes.OrderIcon + " " + classes.SelectedOrderIcon;

  return (
    <div className={classes.DailyContainer}>
      <div className="px-2 text-right">
        <span
          className={orderIconsStyleAsc}
          onClick={(event, desc) => props.onSortClick(event, false)}
        >
          <FontAwesomeIcon icon={faSortAlphaDown} />
        </span>
        <span
          className={orderIconsStyleDesc}
          onClick={(event, desc) => props.onSortClick(event, true)}
        >
          <FontAwesomeIcon icon={faSortAlphaDownAlt} />
        </span>
      </div>
      {props.finEvent &&
        getOrderedAndFormatedData(props.finEvent, props.order).map((el) => {
          const dayBody = el.listOfFinEvents.map((finEv) => {
            let catName = "";

            if (props.category)
              // eslint-disable-next-line array-callback-return
              props.category.map((cat) => {
                if (cat._id === finEv.category) {
                  catName = cat.name;
                }
              });
            return (
              <Row
                key={finEv._id}
                className={props.classes.DataRow + " px-3"}
                onClick={(event, data) => props.onRowClick(event, finEv._id)}
              >
                <Col xs={4} sm={3}>
                  {catName}
                </Col>
                <Col>{finEv.description}</Col>
                <Col
                  xs={4}
                  sm={3}
                  className={
                    finEv.type === "income"
                      ? "text-primary text-right"
                      : "text-danger text-right"
                  }
                >
                  {finEv.amount.toFixed(2)}
                </Col>
              </Row>
            );
          });

          return (
            <DailyView
              key={el.dayOfTheMonth}
              day={el.dayOfTheMonth}
              weekDay={el.weekDay}
              monthYear={el.monthYear}
              incomeSummary={el.incomeSummary}
              expenseSummary={el.expenseSummary}
              onclick={props.onGroupClick}
            >
              {dayBody}
            </DailyView>
          );
        })}
    </div>
  );
};
export default dailyViewWrapper;
