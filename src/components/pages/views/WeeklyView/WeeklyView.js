import React from "react";
import { Card, Row, Col, Badge } from "react-bootstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faSortAlphaDown,
  faSortAlphaDownAlt
} from "@fortawesome/free-solid-svg-icons";
import classes from "./WeeklyView.module.css";
const weeklyView = props => {
  const orderIconsStyleDesc =
    props.order === "desc"
      ? classes.OrderIcon + " " + classes.SelectedOrderIcon
      : classes.OrderIcon;
  const orderIconsStyleAsc =
    props.order === "desc"
      ? classes.OrderIcon
      : classes.OrderIcon + " " + classes.SelectedOrderIcon;
  return (
    <div className={classes.WeeklyViewWrapper}>
      <div className="px-2 text-right">
        <span
          className={orderIconsStyleAsc}
          onClick={(event, desc) => props.onSortClick(event, false)}
        >
          <FontAwesomeIcon icon={faSortAlphaDown} />
        </span>
        <span
          className={orderIconsStyleDesc}
          onClick={(event, desc) => props.onSortClick(event, true)}
        >
          <FontAwesomeIcon icon={faSortAlphaDownAlt} />
        </span>
      </div>
      {props.finEvent &&
        props.finEvent.map(finEW => {
          const badgeClasses = finEW.isCurrentWeek
            ? classes.BadgeBackgroundCurrent + " " + classes.Badge
            : classes.BadgeBackground + " " + classes.Badge;
          return (
            <Card
              key={finEW.startOfWeek}
              onClick={date => props.onClickWeekRow(finEW.endOfWeek)}
              className="w-100 mx-auto mt-2"
            >
              <Card.Header bsPrefix={"card-header p-0 " + classes.Header}>
                <Row>
                  <Col xs={4}>
                    <Badge bsPrefix={badgeClasses}>{finEW.weekLabel}</Badge>
                  </Col>
                  <Col xs={4}>
                    <span className={classes.Summary}>Income: </span>
                    <span className={"text-primary pl-2 " + classes.Summary}>
                      {finEW.incomeSummary.toFixed(2)}
                    </span>
                  </Col>
                  <Col xs={4}>
                    <span className={classes.Summary}>Expense: </span>
                    <span className={"text-danger pl-2 " + classes.Summary}>
                      {finEW.expensesSummary.toFixed(2)}
                    </span>
                  </Col>
                </Row>
              </Card.Header>
            </Card>
          );
        })}
    </div>
  );
};
export default weeklyView;
