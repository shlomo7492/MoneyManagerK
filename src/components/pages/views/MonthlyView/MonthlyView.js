import React from "react";
import { Card, Row, Col, Badge } from "react-bootstrap";
import classes from "./MonthlyView.module.css";
const monthlyView = (props) => {
  let key = 0;
  return (
    <div className={classes.MonthlyViewWrapper}>
      {props.finEvent &&
        props.finEvent.map((finEM) => {
          const badgeClasses = finEM.isCurrentMonth
            ? classes.BadgeBackgroundCurrent + " " + classes.Badge
            : classes.BadgeBackground + " " + classes.Badge;
          return (
            <Card
              key={key++}
              onClick={(date) => props.onclick(finEM.startOfMonth)}
              className="w-100 mx-auto mt-2"
            >
              <Card.Header bsPrefix={"card-header p-0 " + classes.Header}>
                <Row>
                  <Col xs={4}>
                    <Badge bsPrefix={badgeClasses}>{finEM.monthLabel}</Badge>
                  </Col>
                  <Col xs={4}>
                    <span className={classes.Summary}>Income: </span>
                    <span className={"text-primary pl-2 " + classes.Summary}>
                      {finEM.incomeSummary.toFixed(2)}
                    </span>
                  </Col>
                  <Col xs={4}>
                    <span className={classes.Summary}>Expense: </span>
                    <span className={"text-danger pl-2 " + classes.Summary}>
                      {finEM.expensesSummary.toFixed(2)}
                    </span>
                  </Col>
                </Row>
              </Card.Header>
            </Card>
          );
        })}
    </div>
  );
};
export default monthlyView;
