import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faMoneyCheckAlt,
  faMoneyBillWave,
  faFunnelDollar,
  faHandHoldingUsd,
  faHamburger,
  faUsers,
  faBookReader,
  faBusAlt,
  faTheaterMasks,
  faHome,
  faMedkit,
  faUniversity,
  faTableTennis,
  faGifts,
  faReceipt,
  faGift,
  faTshirt,
  faCaretDown,
  faHouseDamage,
  faHourglass,
  faHotel,
  faHeadphonesAlt,
  faHandshake,
  faHeart,
  faHammer,
  faTemperatureHigh,
  faPrayingHands,
  faMugHot,
  faCandyCane,
  faRoad,
  faGlasses,
  faAward,
  faPlaneDeparture,
  faSwimmer,
  faMountain,
  faPlaceOfWorship,
  faPeopleCarry,
  faChess,
  faGlassCheers,
  faShoppingCart,
  faShippingFast,
  faLightbulb,
  faCouch,
  faBabyCarriage,
  faDice,
  faChargingStation,
  faMicrochip,
  faSadTear,
  faBirthdayCake,
  faSnowman,
} from "@fortawesome/free-solid-svg-icons";
import { faCaretSquareRight } from "@fortawesome/free-regular-svg-icons";
import { Form, FormCheck } from "react-bootstrap";
import CollapsibleArea from "../../containers/CollapsibleComponent/Collapsible";
import AddNewCategory from "../../containers/CategoryForm/AddNewCategory";
const categories = (props) => {
  const icons = {
    faMoneyCheckAlt: faMoneyCheckAlt,
    faMoneyBillWave: faMoneyBillWave,
    faFunnelDollar: faFunnelDollar,
    faHandHoldingUsd: faHandHoldingUsd,
    faHamburger: faHamburger,
    faUsers: faUsers,
    faBookReader: faBookReader,
    faBusAlt: faBusAlt,
    faTheaterMasks: faTheaterMasks,
    faHome: faHome,
    faMedkit: faMedkit,
    faUniversity: faUniversity,
    faTableTennis: faTableTennis,
    faGifts: faGifts,
    faGift: faGift,
    faReceipt: faReceipt,
    faTshirt: faTshirt,
    faCaretSquareRight: faCaretSquareRight,
    faCaretDown: faCaretDown,
    faHouseDamage: faHouseDamage,
    faHourglass: faHourglass,
    faHotel: faHotel,
    faHeadphonesAlt: faHeadphonesAlt,
    faHandshake: faHandshake,
    faHeart: faHeart,
    faHammer: faHammer,
    faTemperatureHigh: faTemperatureHigh,
    faPrayingHands: faPrayingHands,
    faMugHot: faMugHot,
    faCandyCane: faCandyCane,
    faRoad: faRoad,
    faGlasses: faGlasses,
    faAward: faAward,
    faPlaneDeparture: faPlaneDeparture,
    faSwimmer: faSwimmer,
    faMountain: faMountain,
    faPlaceOfWorship: faPlaceOfWorship,
    faPeopleCarry: faPeopleCarry,
    faChess: faChess,
    faGlassCheers: faGlassCheers,
    faShoppingCart: faShoppingCart,
    faShippingFast: faShippingFast,
    faLightbulb: faLightbulb,
    faCouch: faCouch,
    faBabyCarriage: faBabyCarriage,
    faDice: faDice,
    faChargingStation: faChargingStation,
    faMicrochip: faMicrochip,
    faSadTear: faSadTear,
    faBirthdayCake: faBirthdayCake,
    faSnowman: faSnowman,
  };
  const containerStyle = props.openCategories
    ? props.classes.CategoriesContainer +
      " " +
      props.classes.CategoriesContainerExtended
    : props.classes.CategoriesContainer;

  let chosenCategory = "";
  props.categories.map((cat) => {
    if (props.currentCategory === cat._id) {
      chosenCategory = cat.name;
    }
  });
  // const categoryMsg =
  //   props.currentCategory.length > 0 ? chosenCategory : props.errorMsg;
  // const caretArrowStyle = props.openCategories
  //   ? props.classes.LargeFAIcon + " " + props.classes.CaretArrowUp
  //   : props.classes.LargeFAIcon + " " + props.classes.CaretArrowDown;

  return (
    <Form.Group className={containerStyle + " h-auto"}>
      <CollapsibleArea
        title={"Categories: "}
        categoryName={chosenCategory}
        errorMsg={props.errorMsg}
      >
        <div className="d-flex flex-row flex-wrap justify-content-start w-100 h-auto">
          {props.categories.map((cat) => {
            const classCheckedButton =
              props.currentCategory === cat._id
                ? props.classes.RadioButton +
                  " " +
                  props.classes.CheckedRadioButton
                : props.classes.RadioButton;
            return (
              <div key={cat._id} className={props.classes.CategoriesLabel}>
                <FormCheck bsPrefix={classCheckedButton}>
                  <FormCheck.Input
                    type={"radio"}
                    name="category"
                    id={cat._id}
                    value={cat._id}
                    checked={props.currentCategory === cat._id}
                    onChange={(event) => props.onchange(event)}
                  />
                  <FormCheck.Label htmlFor={cat._id} bsPrefix={"none"}>
                    <FontAwesomeIcon icon={icons[cat.icon]} className="mr-1" />
                    {cat.name}
                  </FormCheck.Label>
                </FormCheck>
              </div>
            );
          })}
        </div>
        <AddNewCategory
          classes={props.classes}
          categoryType={props.categoryType}
          onAddCategory={props.onAddCategory}
        />
      </CollapsibleArea>
    </Form.Group>
  );
};
export default categories;
