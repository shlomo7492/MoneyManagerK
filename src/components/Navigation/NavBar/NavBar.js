import React from "react";
import moment from "moment";
import { withRouter } from "react-router-dom";
import { Navbar, Nav } from "react-bootstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faCalendarAlt,
  faCalendarDay,
  faCalendarWeek,
  faMoon,
  faMoneyCheckAlt
} from "@fortawesome/free-solid-svg-icons";
import Logo from "../Logo/Logo";

import classes from "./NavBar.module.css";

const navBar = props => {
  // eslint-disable-next-line no-unused-expressions
  const selectHandler = path => {
    props.history.push(path);
  };
  const selectDateUrlParam = moment(
    props.selectDate,
    moment.ISO_8601,
    true
  ).isValid()
    ? typeof props.selectDate !== "undefined"
      ? moment(props.selectDate).format("YYYY-MM-DD")
      : moment().format("YYYY-MM-DD")
    : moment().format("YYYY-MM-DD");
  return (
    <Navbar
      collapseOnSelect
      expand="md"
      bg={classes.NavBar}
      variant="dark"
      className={classes.NavBar}
    >
      <Navbar.Brand href="/">
        <Logo> Money Manager K</Logo>
      </Navbar.Brand>
      <Navbar.Toggle aria-controls="responsive-navbar-nav" />
      <Navbar.Collapse id="responsive-navbar-nav">
        <Nav className="mr-auto" onSelect={eventKey => selectHandler(eventKey)}>
          <Nav.Link eventKey={`/daily/${selectDateUrlParam}`}>
            <FontAwesomeIcon icon={faCalendarDay} /> Daily
          </Nav.Link>
          <Nav.Link eventKey={`/calendar/${selectDateUrlParam}`}>
            <FontAwesomeIcon icon={faCalendarAlt} /> Calendar
          </Nav.Link>
          <Nav.Link eventKey={`/weekly/${selectDateUrlParam}`}>
            <FontAwesomeIcon icon={faCalendarWeek} /> Weekly
          </Nav.Link>
          <Nav.Link eventKey={`/monthly/${selectDateUrlParam}`}>
            <FontAwesomeIcon icon={faMoon} /> Monthly
          </Nav.Link>
          <Nav.Link eventKey={`/total/${selectDateUrlParam}`}>
            <FontAwesomeIcon icon={faMoneyCheckAlt} /> Total
          </Nav.Link>
        </Nav>
      </Navbar.Collapse>
    </Navbar>
  );
};

export default withRouter(navBar);
