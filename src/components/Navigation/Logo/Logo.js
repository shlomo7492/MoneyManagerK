import React from "react";
import Logo from "../../../assets/images/coinsLogo.png";

import classes from "./Logo.module.css";

const logo = props => {
  return (
    <div className={classes.Logo}>
      <span onClick={props.openModal}>
        <img
          src={Logo}
          width="40px"
          alt={props.children}
          title={props.children}
        />
      </span>
      {props.children}
    </div>
  );
};

export default logo;
