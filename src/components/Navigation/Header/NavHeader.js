import React, { Component } from "react";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";

import {
  //MODAL NO MORE IN USE
  //modalManage,
  logOut,
  updateDisplayBalance,
} from "../../../redux/actions/appActions";
import NavBar from "../NavBar/NavBar";
import NavTitle from "../NavTitle/NavTitle";
import DisplaySummary from "../../DisplaySummary/DisplaySummary";

import classes from "./NavHeader.module.css";

class NavHeader extends Component {
  //MODAL NO MORE IN USE
  // showModal = () => {
  //   this.props.modalManage(true, "AppInfo");
  // };
  // closeModal = () => {
  //   this.props.modalManage(false, "");
  // };
  logoutHandler = () => {
    this.props.logOut();
  };

  render() {
    return (
      <div className={classes.NavHeader}>
        <NavTitle logoutHandle={this.logoutHandler} {...this.props} />
        <div className={classes.NavBarContainer}>
          <NavBar selectDate={this.props.selectDate} />
        </div>
        <div>
          {!this.props.isForm && (
            <DisplaySummary
              summary={this.props.finEventSummary.toJS()}
              finEvent={this.props.finEvent}
            />
          )}
        </div>
      </div>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    finEventSummary: state.appState.get("finEventSummary"),
    selectDate: state.appState.get("selectDate"),
    isForm: state.appState.get("isForm"),
    finEvent: state.financialEventState.get("finEvent"),
  };
};
const mapDispatchToProps = {
  //MODAL NO MORE IN USE
  //modalManage: modalManage,
  logOut: logOut,
  displayBalance: updateDisplayBalance,
};
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(NavHeader));
