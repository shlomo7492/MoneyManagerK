import React from "react";
import { NavLink } from "react-router-dom";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faSignOutAlt, faUserCircle } from "@fortawesome/free-solid-svg-icons";
import DateSelector from "../../../containers/DateSelector/DateSelector";

import classes from "./NavTitle.module.css";

const NavTitle = props => {
  let user = JSON.parse(localStorage.getItem("user"));
  if (!user) user = { userName: "No user" };
  return (
    <div className={classes.NavTitle}>
      {!props.isForm ? <DateSelector {...props} /> : <div></div>}
      <div className={classes.LinksContainer}>
        <NavLink to="/user" title={"Profile"}>
          <FontAwesomeIcon icon={faUserCircle} />
        </NavLink>
        <NavLink
          to={{
            pathname: "/logout",
            currentLocation: props.history.location.pathname
          }}
          onClick={props.logoutHandle}
          title="Logout"
        >
          <FontAwesomeIcon icon={faSignOutAlt} />
        </NavLink>
      </div>
    </div>
  );
};

export default NavTitle;
