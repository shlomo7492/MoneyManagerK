import React from "react";

import Add from "../Button/Add";
import classes from "./AddButton.module.css";

const AddButton = props => {
  return (
    <div className={classes.Container}>
      <Add onclick={props.onclick} />
    </div>
  );
};

export default AddButton;
