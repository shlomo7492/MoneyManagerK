import React from "react";
import ctx from "classnames";
import classes from "./Modal.module.css";

const modal = props => {
  const classNames = ctx(
    classes.ModalContainer,
    { [classes.ModalHide]: !props.show },
    { [classes.ModalShow]: props.show }
  );
  return (
    <div className={classNames}>
      <div>
        <button onClick={props.onclick}>X</button>
      </div>
      <div className={classes.ModalContent}>{props.children}</div>
    </div>
  );
};
export default modal;
