import React, { Component } from "react";
import DatePicker from "react-datepicker";
import classes from "./DatePicker.module.css";
import "react-datepicker/dist/react-datepicker.css";

class MyDatePicker extends Component {
  state = {
    startDate: ""
  };

  handleClick = (event, date) => {
    //event.persist();
  };
  componentDidMount = () => {
    this.props.defaultValue &&
      this.setState({ startDate: this.props.defaultValue });
  };
  render() {
    return (
      <div className={classes.Calendar}>
        <DatePicker
          dateFormat="yyyy-MM-dd"
          timeCaption="date"
          selected={this.state.startDate}
          onClick={date => this.handleClick(date)}
          onChange={date => this.props.onchange(date)}
          shouldCloseOnSelect={true}
          className={classes.CalendarInput}
          popperPlacement="middle-start"
          popperModifiers={{
            offset: {
              enabled: true,
              offset: "10px, 15px"
            },
            preventOverflow: {
              enabled: true,
              escapeWithReference: false, // force popper to stay in viewport (even when input is scrolled out of view)
              boundariesElement: "viewport"
            }
          }}
        />
      </div>
    );
  }
}
export default MyDatePicker;
