import React from "react";
import { Card } from "react-bootstrap";
import classes from "./DisplaySummary.module.css";

const DisplaySummary = props => {
  return (
    <Card bsPrefix={"text-secondary bg-light " + classes.Container}>
      <Card.Body
        bsPrefix={"text-center " + classes.CardFlex + " " + classes.CardFont}
      >
        <div className={classes.Summary}>
          <span className="pr-2">Income:</span>
          <span className="text-primary ">
            {props.summary.income.toFixed(2)}
          </span>
        </div>
        <div className={classes.Summary}>
          <span className="pr-2">Expenses:</span>
          <span className="text-danger">
            {props.summary.expense.toFixed(2)}
          </span>
        </div>
        <div className={classes.Summary}>
          <span className="pr-2">Balance:</span>
          <span className="text-dark">{props.summary.balance.toFixed(2)}</span>
        </div>
      </Card.Body>
    </Card>
  );
};

export default DisplaySummary;
