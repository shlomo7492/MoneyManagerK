import React from "react";
import ctx from "classnames";

import classes from "./BlurBackground.module.css";

const blurBackground = props => {
  const classNames = ctx(classes.BlurBackground, {
    [classes.BlurBackgroundShow]: props.show,
    [classes.BlurBackgroundHide]: !props.show
  });
  return <div className={classNames} onClick={props.onclick}></div>;
};
export default blurBackground;
