import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import MainLayoutBody from "./MainLayoutBody";
import classes from "./MainLayout.module.css";
import { connect } from "react-redux";
import { changeCurrentCategory } from "../../redux/actions/categoryActions";

class MainLayout extends Component {
  componentDidUpdate = () => {
    if (!this.props.isForm) {
      this.props.changeCurrentCategory("");
    }
  };
  render() {
    return (
      <MainLayoutBody isLoggedIn={this.props.isLoggedIn} classes={classes.Main}>
        {this.props.children}
      </MainLayoutBody>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    isLoggedIn: state.appState.get("loggedIn"),
    isForm: state.appState.get("isForm"),
  };
};
const mapDispatchToProps = { changeCurrentCategory: changeCurrentCategory };
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(MainLayout));
