import React from "react";
import NavHeader from "../Navigation/Header/NavHeader";
import LoginForm from "../../containers/UserForm/LoginForm";

const mainLayoutBody = props => {
  return (
    <>
      {props.isLoggedIn ? (
        <div className={props.classes}>
          <NavHeader />
          {props.children}
        </div>
      ) : (
        <LoginForm />
      )}
    </>
  );
};
export default mainLayoutBody;
