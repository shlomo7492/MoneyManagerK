//to be shown in the modal opened on AddButton click
//closes the parent modal and open new modal with
//AddAmountForm component in it that is connected to the store...
import React from "react";
import ctx from "classnames";
import classes from "./Button.module.css";

const AddAmount = props => {
  const btnClass = ctx(classes.AddAmount, {
    [classes.GreenBtn]: props.name === "inc",
    [classes.RedBtn]: props.name === "exp"
  });
  const content = props.name === "inc" ? "+ Add Income" : "- Add Expense";
  return (
    <div>
      <button
        onClick={props.onclick}
        className={btnClass}
        style={{ backgroundColor: props.color }}
      >
        {content}
      </button>
    </div>
  );
};
export default AddAmount;
