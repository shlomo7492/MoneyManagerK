import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPlus } from "@fortawesome/free-solid-svg-icons";
import classes from "./Button.module.css";

const Add = props => {
  return (
    <div className={classes.Add} onClick={props.onclick}>
      <FontAwesomeIcon icon={faPlus} />
    </div>
  );
};

export default Add;
