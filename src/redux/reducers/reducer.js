import FinancialEventReducer from "./financialEvent/financialEvent";
import CategoryReducer from "./category/categoryReducer";
import AppReducer from "./app/appReducer";

import { combineReducers } from "redux";

const reducer = combineReducers({
  financialEventState: FinancialEventReducer,
  categoryState: CategoryReducer,
  appState: AppReducer
});

export default reducer;
