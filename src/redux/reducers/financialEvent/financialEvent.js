import {
  ADD_FIN_EVENT,
  REMOVE_FIN_EVENT_FROM_STATE,
  ADD_PREV_MONTH_SUMMARY_BY_TYPE,
} from "../../actions/finEventActions";
import { fromJS } from "immutable";
import {
  addFinEvent,
  addPrevMonthSummaryByType,
  removeFinEventFromState,
} from "../../../services/financialEventService/finEventUtilities";
export const initialState = fromJS({
  finEvent: [],
  prevMonthExpenses: 0,
});

const finEventReducer = (state = initialState, action) => {
  switch (action.type) {
    case ADD_FIN_EVENT:
      return addFinEvent(action, state);
    case REMOVE_FIN_EVENT_FROM_STATE:
      return removeFinEventFromState(state);
    case ADD_PREV_MONTH_SUMMARY_BY_TYPE:
      return addPrevMonthSummaryByType(action, state);
    default:
      return state;
  }
};

export default finEventReducer;
