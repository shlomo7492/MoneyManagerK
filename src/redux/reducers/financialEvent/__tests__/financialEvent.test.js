import { initialState } from "../financialEvent";
import reducer from "../financialEvent";
import {
  ADD_FIN_EVENT,
  REMOVE_FIN_EVENT_FROM_STATE,
  ADD_PREV_MONTH_SUMMARY_BY_TYPE,
} from "../../../actions/finEventActions";
import { List } from "immutable";

describe("Testing finEvent reducer action methods", () => {
  it("Default reducer action test", () => {
    expect(reducer(undefined, {})).toEqual(initialState);
  });
  it("ADD_FIN_EVENT action test", () => {
    const finEventArr = [
      {
        __v: 0,
        _id: "5e5ffc798cf06b43883dcb48",
        amount: 1000,
        category: "5e5e28dd6cff4b1c788995dd",
        date: "2020-03-04T19:07:37.987Z",
        description: "advance money",
        type: "income",
        userId: "5e0305dc3dffe24c5c32edeb",
      },
      {
        __v: 0,
        _id: "5e6010018cf06b43883dcb49",
        amount: 1500,
        category: "5e5e28dd6cff4b1c788995dd",
        date: "2020-03-04T20:30:57.853Z",
        description: "pay check 1",
        type: "income",
        userId: "5e0305dc3dffe24c5c32edeb",
      },
    ];
    const finEventArrTestExpectations = [
      {
        __v: 0,
        _id: "5e5ffc798cf06b43883dcb48",
        amount: 1000,
        category: "5e5e28dd6cff4b1c788995dd",
        date: "2020-03-04T19:07:37.987Z",
        description: "advance money",
        type: "income",
        userId: "5e0305dc3dffe24c5c32edeb",
      },
      {
        __v: 0,
        _id: "5e6010018cf06b43883dcb49",
        amount: 1500,
        category: "5e5e28dd6cff4b1c788995dd",
        date: "2020-03-04T20:30:57.853Z",
        description: "pay check 1",
        type: "income",
        userId: "5e0305dc3dffe24c5c32edeb",
      },
    ];
    const actionData = { accessToken: "SomethingLikeToken", data: finEventArr };
    expect(
      reducer(initialState, {
        type: ADD_FIN_EVENT,
        data: actionData,
      })
    ).toEqual(
      initialState.setIn(["finEvent"], List([...finEventArrTestExpectations]))
    );
  });
  it("REMOVE_FIN_EVENT_FROM_STATE action test", () => {
    expect(
      reducer(initialState, {
        type: REMOVE_FIN_EVENT_FROM_STATE,
      })
    ).toEqual(initialState.setIn(["finEvent"], List([])));
  });
  it("ADD_PREV_MONTH_SUMMARY_BY_TYPE action test", () => {
    const finEventArr = [
      {
        __v: 0,
        _id: "5e5ffc798cf06b43883dcb48",
        amount: 1000,
        category: "5e5e28dd6cff4b1c788995dd",
        date: "2020-03-04T19:07:37.987Z",
        description: "advance money",
        type: "income",
        userId: "5e0305dc3dffe24c5c32edeb",
      },
      {
        __v: 0,
        _id: "5e6010018cf06b43883dcb49",
        amount: 1500,
        category: "5e5e28dd6cff4b1c788995dd",
        date: "2020-03-04T20:30:57.853Z",
        description: "pay check 1",
        type: "income",
        userId: "5e0305dc3dffe24c5c32edeb",
      },
    ];

    const actionData = { accessToken: "SomethingLikeToken", data: finEventArr };

    expect(
      reducer(initialState, {
        type: ADD_PREV_MONTH_SUMMARY_BY_TYPE,
        data: actionData,
      })
    ).toEqual(initialState.setIn(["prevMonthExpenses"], 2500));
  });
});
