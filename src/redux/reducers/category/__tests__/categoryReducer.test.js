import { initialState } from "../categoryReducer";
import reducer from "../categoryReducer";
import { createCategory } from "../../../../services/categoryService/categoryUtilities";
import { List } from "immutable";
import {
  ADD_CATEGORY,
  REMOVE_CATEGORY_FROM_STATE,
  CREATE_CATEGORY,
  CHANGE_CURRENT_CATEGORY,
} from "../../../actions/categoryActions";

describe("Testing category reducer action methods", () => {
  it("Create initial reducer", async () => {
    expect(reducer(undefined, {})).toEqual(initialState);
  });
  it("ADD_CATEGORY action test", async () => {
    const testCategories = [
      {
        _id: "5e5e28dd6cff4b1c788995de",
        icon: "faMoneyBillWave",
        name: "Petty cash",
        type: "income",
        userId: "default",
      },
      {
        _id: "5e5e28dd6cff4b1c788995df",
        icon: "faGift",
        name: "Bonus",
        type: "income",
        userId: "default",
      },
    ];
    expect(
      reducer(initialState, {
        type: ADD_CATEGORY,
        data: { token: "testAccessToken", data: testCategories },
      })
    ).toEqual(initialState.setIn(["category"], List(testCategories)));
  });
  it("CREATE_CATEGORY action test", async () => {
    const testCategory = {
      _id: "5e5e28dd6cff4b1c788995de",
      icon: "faMoneyBillWave",
      name: "Petty cash",
      type: "income",
      userId: "default",
    };
    const testCategoryExpected = {
      _id: "5e5e28dd6cff4b1c745655de",
      icon: "faMoneyBill",
      name: "Something else",
      type: "expence",
      userId: "default",
    };
    const expectedAction = {
      type: CREATE_CATEGORY,
      data: {
        accessToken:
          "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYW1lIjoiTmlraSIsInVzZXJJZCI6IjVlMDMwNWRjM2RmZmUyNGM1YzMyZWRlYiIsInVzZXJFbWFpbCI6Im5pa29AbWFpbC5jb20iLCJpYXQiOjE1OTkyMTYzOTIsImV4cCI6MTU5OTIxNjY5Mn0.Nb9Ho7qqsa7Xf69ZUE_6NWyBS6QwmLehV8nah_PvBMs",
        cookies: "undefined",
        data: testCategory, //Expected,
      },
    };

    const expectedState = createCategory(initialState, expectedAction);
    expect(reducer(initialState, expectedAction)).toEqual(
      initialState
        .setIn(["category"], expectedState.getIn(["category"]))
        .setIn(["currentCategory"], testCategory._id)
    );
  });
  it("CHANGE_CURRENT_CATEGORY action test", async () => {
    expect(
      reducer(initialState, {
        type: CHANGE_CURRENT_CATEGORY,
        data: "dak340f9dsfjkasr3r",
      })
    ).toEqual(initialState.setIn(["currentCategory"], "dak340f9dsfjkasr3r"));
  });
  it("REMOVE_CATEGORY_FROM_STATE action test", async () => {
    expect(
      reducer(initialState, {
        type: REMOVE_CATEGORY_FROM_STATE,
      })
    ).toEqual(
      initialState.setIn(["category"], List()).setIn(["currentCategory"], "")
    );
  });
});
