import {
  ADD_CATEGORY,
  REMOVE_CATEGORY_FROM_STATE,
  CREATE_CATEGORY,
  CHANGE_CURRENT_CATEGORY,
} from "../../actions/categoryActions";
import { fromJS } from "immutable";
import {
  addCategories,
  createCategory,
  changeCurrentCategory,
  clearCategoryFromState,
} from "../../../services/categoryService/categoryUtilities";
export const initialState = fromJS({
  category: [],
  currentCategory: "",
});

const categoryReducer = (state = initialState, action) => {
  //console.log("catReducer 18", action);

  switch (action.type) {
    case ADD_CATEGORY:
      //return state.set("category", addCategories(action));
      return addCategories(state, action);
    case CREATE_CATEGORY:
      return createCategory(state, action);
    case CHANGE_CURRENT_CATEGORY:
      return changeCurrentCategory(state, action.data);
    case REMOVE_CATEGORY_FROM_STATE:
      return clearCategoryFromState(state);
    default:
      return state;
  }
};

export default categoryReducer;
