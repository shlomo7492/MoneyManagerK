import {
  CHANGE_SELECT_DATE,
  //Modal is no more in use
  //MANAGE_MODAL,
  CURRENT_BALANCE,
  LOGOUT,
  LOGIN,
  REMOVE_CURRENT_BALANCE_FROM_STATE,
  MANAGE_ISFORM_STATE,
  GET_USER,
} from "../../actions/appActions";
import { fromJS } from "immutable";
import moment from "moment";
import { isSessionAlive } from "../../../services/utilityService/utilityService";
import {
  changeSelectDate,
  updateCurrentBalance,
  onLoginAction,
  onLogOut,
  onClearCurrentBalance,
  onManageIsFormState,
  setUser,
} from "../../../services/appReducerUtilities/appReducersUtilities";
export const initialState = fromJS({
  selectDate: new Date(moment().format("DD MMMM,YYYY 00:00:00").toString()),
  dateType: "month",
  //modal is currently not in use
  //modal: {
  //  show: false,
  //  type: "",
  //  data: "",
  //},
  finEventSummary: {
    income: 0,
    expense: 0,
    balance: 0,
  },
  loggedIn: isSessionAlive(),
  user: {},
  isForm: false,
});

//Actual reducer function for appState
const appReducer = (state = initialState, action) => {
  switch (action.type) {
    case CHANGE_SELECT_DATE:
      return changeSelectDate(state, action);
    // MANAGE_MODAL action is no more used
    //case MANAGE_MODAL:
    //  return manageModalShow(state, action);
    case CURRENT_BALANCE:
      return updateCurrentBalance(state, action);
    case LOGIN:
      return onLoginAction(state, action);
    case GET_USER:
      return setUser(state, action);
    case LOGOUT:
      return onLogOut(state, action);
    case REMOVE_CURRENT_BALANCE_FROM_STATE:
      return onClearCurrentBalance(state);
    case MANAGE_ISFORM_STATE:
      return onManageIsFormState(state, action);
    default: {
      return state;
    }
  }
};

export default appReducer;
