import { initialState } from "../appReducer";
import {
  CHANGE_SELECT_DATE,
  // MANAGE_MODAL, MODAL IS NO MORE IN USE
  CURRENT_BALANCE,
  LOGOUT,
  LOGIN,
  REMOVE_CURRENT_BALANCE_FROM_STATE,
  MANAGE_ISFORM_STATE,
  GET_USER,
} from "../../../actions/appActions";
import moment from "moment";
import { Map } from "immutable";
import reducer from "../appReducer";
//import expect from "expect";

describe("Testing application reducer action methods", () => {
  it("Create initial reducer", async () => {
    expect(reducer(undefined, {})).toEqual(initialState);
  });
  it("Change selectDate", async () => {
    const date = new Date(moment("2020-10-03"));
    console.log(23, date);
    expect(
      reducer(initialState, {
        type: CHANGE_SELECT_DATE,
        data: { selectDate: date, dateType: "month" },
      })
    ).toEqual(
      initialState.setIn(["selectDate"], date).setIn(["dateType"], "month")
    );
  });
  it("Update current balance", async () => {
    const finEvent = [
      { type: "income", amount: 200 },
      { type: "expense", amount: 20 },
      { type: "income", amount: 150 },
      { type: "expense", amount: 15 },
      { type: "expense", amount: 25 },
    ];
    expect(
      reducer(initialState, {
        type: CURRENT_BALANCE,
        data: { type: "daily", finEvent: finEvent },
      })
    ).toEqual(
      initialState.setIn(
        ["finEventSummary"],
        Map({
          income: 350,
          expense: 60,
          balance: 290,
        })
      )
    );
  });
  it("Logging in action test", async () => {
    expect(
      reducer(initialState, {
        type: LOGIN,
        data: {
          accessToken: "This is test token",
          data: { userName: "Test name", userEmail: "test@email.it" },
        },
      })
    ).toEqual(
      initialState
        .setIn(["loggedIn"], true)
        .setIn(
          ["user"],
          Map({ userName: "Test name", userEmail: "test@email.it" })
        )
    );
  });
  it("Getting user data action test", async () => {
    expect(
      reducer(initialState, {
        type: GET_USER,
        data: {
          accessToken: "This is test token",
          data: {
            userName: "Test name",
            userEmail: "test@email.it",
            userId: "testUserId",
          },
        },
      })
    ).toEqual(
      initialState.setIn(
        ["user"],
        Map({
          userName: "Test name",
          userEmail: "test@email.it",
          userId: "testUserId",
        })
      )
    );
  });
  it("Logout action test", async () => {
    expect(
      reducer(initialState, {
        type: LOGOUT,
      })
    ).toEqual(
      initialState
        .setIn(["selectDate"], new Date(moment().format("YYYY-MM-DD")))
        .setIn(["loggedIn"], false)
        .setIn(["user"], Map({}))
    );
  });
  it("Clear current balance action test", async () => {
    expect(
      reducer(initialState, {
        type: REMOVE_CURRENT_BALANCE_FROM_STATE,
      })
    ).toEqual(
      initialState.setIn(
        ["finEventSummary"],
        Map({ income: 0, expense: 0, balance: 0 })
      )
    );
  });
  it("MANAGE_ISFORM_STATE action test", async () => {
    expect(
      reducer(initialState, {
        type: MANAGE_ISFORM_STATE,
        data: false,
      })
    ).toEqual(initialState.setIn(["isForm"], false));
  });
});
