import {
  getCategory,
  createNewCategory,
  editCategory,
  deleteOneCategory,
} from "../../services/categoryService/categoryService";

/* CATEGORIES RELATED  ACTIONS*/
export const ADD_CATEGORY = "ADD_CATEGORY";
export function addCategory(data) {
  return function (dispatch) {
    getCategory(data, dispatch).then((res) => {
      dispatch({ type: ADD_CATEGORY, data: res });
    });
  };
}
/* REMOVE CATEGORY FOR THE LOGGED IN USER FROM THE STORE 
   FOR EXAMPLE ON LOGOUT*/
export const REMOVE_CATEGORY_FROM_STATE = "REMOVE_CATEGORY_FROM_STATE";
export function removeCategoryFromState() {
  return {
    type: REMOVE_CATEGORY_FROM_STATE,
  };
}
//CREATE NEW CATEGORY
export const CREATE_CATEGORY = "CREATE_CATEGORY";
export function createCategory(data) {
  return function (dispatch) {
    createNewCategory(data, dispatch).then((res) => {
      if (res) dispatch({ type: CREATE_CATEGORY, data: res });
    });
  };
}
export const CHANGE_CURRENT_CATEGORY = "CHANGE_CURRENT_CATEGORY";
export function changeCurrentCategory(data) {
  return { type: CHANGE_CURRENT_CATEGORY, data: data };
}

//UPDATE CATEGORY
export const UPDATE_CATEGORY = "UPDATE_CATEGORY";
export function updateCategory(data) {
  return function (dispatch) {
    editCategory(data, dispatch).then((res) => {
      dispatch({ type: UPDATE_CATEGORY, data: res });
    });
  };
}
//DELETE CATEGORY
export const DELETE_CATEGORY = "DELETE_CATEGORY";
export function deleteCategory(data) {
  return function (dispatch) {
    deleteOneCategory(data, dispatch).then((res) => {
      dispatch({ type: DELETE_CATEGORY, data: res });
    });
  };
}
