import {
  getUserData,
  userLogin,
  registerUser,
  editUserData,
  deleteUserData,
} from "../../services/userService/userService";

/*APP RELATED ACTIONS*/
//CHANGE_SELECT_DATE
export const CHANGE_SELECT_DATE = "CHANGE_SELECT_DATE";
export function selectDateChange(selectDate, dateType) {
  return {
    type: CHANGE_SELECT_DATE,
    data: { selectDate: selectDate, dateType: dateType },
  };
}

//MODAL SHOW/HIDE AND POPULATE CONTENT !!! NO MORE IN USE !!!
// export const MANAGE_MODAL = "MANAGE_MODAL";

// export function modalManage(show, type, data) {
//   return {
//     type: MANAGE_MODAL,
//     data: { show: show, type: type, data: data },
//   };
// }

//CURRENT_BALANCE
export const CURRENT_BALANCE = "CURRENT_BALANCE";
export function updateDisplayBalance(type, finEvent) {
  return {
    type: CURRENT_BALANCE,
    data: { type: type, finEvent: finEvent },
  };
}

//REMOVE CURRENT_BALANCE FROM STATE
export const REMOVE_CURRENT_BALANCE_FROM_STATE =
  "REMOVE_CURRENT_BALANCE_FROM_STATE";
export function clearDisplayBalance() {
  return {
    type: REMOVE_CURRENT_BALANCE_FROM_STATE,
  };
}

//LOGIN(USER) RELATED ACTIONS
//Get USER
export const GET_USER = "GET_USER";
export function getUser(data) {
  let reqData = {};
  data.token === null ? (reqData.token = "") : (reqData = { ...data });
  return function (dispatch) {
    getUserData(reqData, dispatch).then((res) => {
      if (res) {
        console.log(res);
        dispatch({ type: GET_USER, data: res });
      }
    });
  };
}
//LOGIN
export const LOGIN = "LOGIN";
export function logIn(data, location) {
  return function (dispatch) {
    userLogin(data, dispatch).then((res) => {
      dispatch({ type: LOGIN, data: res });
    });
  };
}
//LOGOUT
export const LOGOUT = "LOGOUT";
export function logOut() {
  return { type: LOGOUT };
}

//USER REGISTER ACCOUNT
export const USER_REGISTER = "USER_REGISTER";
export function registerNewUser(data) {
  return function (dispatch) {
    registerUser(data).then((res) => {
      dispatch({ type: USER_REGISTER, data: res });
    });
  };
}
//USER UPDATE
export const USER_UPDATE = "USER_UPDATE";
export function updateUser(data) {
  return function (dispatch) {
    editUserData(data, dispatch).then((res) => {
      dispatch({ type: USER_UPDATE, data: res });
    });
  };
}

//USER DELETE ACCOUNT
export const USER_DELETE = "USER_DELETE";
export function deleteUser(data) {
  return function (dispatch) {
    deleteUserData(data, dispatch).then((res) => {
      dispatch({ type: USER_DELETE, data: res });
    });
  };
}

//MANAGE_ISFORM_STATE
export const MANAGE_ISFORM_STATE = "MANAGE_ISFORM_STATE";
export function manageIsFormState(isForm) {
  return { type: MANAGE_ISFORM_STATE, data: isForm };
}
