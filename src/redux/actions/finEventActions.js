import {
  getFinEvent,
  createFinEvent,
  updateFinEvent,
  deleteFinEvent,
} from "../../services/financialEventService/finEventService";

/* FINANCIAL EVENTS ACTIONS */
export const ADD_FIN_EVENT = "ADD_FIN_EVENT";
export function addFinEvent(data) {
  return function (dispatch) {
    getFinEvent(data, dispatch).then((res) => {
      dispatch({
        type: ADD_FIN_EVENT,
        data: res,
      });
    });
  };
}
/* REMOVE FINANCIAL EVENTS FOR THE LOGGED IN USER FROM THE STORE 
   FOR EXAMPLE ON LOGOUT*/
export const REMOVE_FIN_EVENT_FROM_STATE = "REMOVE_FIN_EVENT_FROM_STATE";
export function removeFinEventFromState() {
  return {
    type: REMOVE_FIN_EVENT_FROM_STATE,
  };
}
// CREATE NEW FINANCIAL EVENT
export const CREATE_FIN_EVENT = "CREATE_FIN_EVENT";
export function createNewFinEvent(data) {
  return function (dispatch) {
    createFinEvent(data, dispatch).then((res) =>
      dispatch({ type: CREATE_FIN_EVENT, data: res })
    );
  };
}

//EDIT FINANCIAL EVENT
export const EDIT_FIN_EVENT = "EDIT_FIN_EVENT";
export function updateOneFinEvent(data) {
  return function (dispatch) {
    updateFinEvent(data, dispatch).then((res) =>
      dispatch({ type: EDIT_FIN_EVENT, data: res })
    );
  };
}
//DELETE FINANCIAL EVENT
export const DELETE_FIN_EVENT = "DELETE_FIN_EVENT";
export function deleteOneFinEvent(data) {
  return function (dispatch) {
    deleteFinEvent(data, dispatch).then((res) =>
      dispatch({ type: DELETE_FIN_EVENT, data: res })
    );
  };
}
//Add financialEvent summary of type income/expense
//for the previous month to the this.state.
//ADD FINANCIAL EVENT SUMMARY BY TYPE FOR PREV MONTH
export const ADD_PREV_MONTH_SUMMARY_BY_TYPE = "ADD_PREV_MONTH_SUMMARY_BY_TYPE";
export function addPrevMonthSummary(data) {
  return function (dispatch) {
    getFinEvent(data, dispatch).then((res) => {
      dispatch({
        type: ADD_PREV_MONTH_SUMMARY_BY_TYPE,
        data: res,
      });
    });
  };
}
