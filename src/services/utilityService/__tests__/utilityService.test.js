import { getDateRange } from "../utilityService";
const dateRangeMonthly = getDateRange("monthly", new Date("2020-04-01"));
const dateRangeWeekly = getDateRange("weekly", new Date("2020-04-04"));
const dateRangeDaily = getDateRange("daily", new Date("2020-04-04"));

//DateRange tests
describe("Testing utilityServices", () => {
  it("Date range test for monthly view - start date", () => {
    expect(dateRangeMonthly.start).toBe("2020-01-01 00:00:00");
  });
  it("Date range test for monthly view - end date", () => {
    expect(dateRangeMonthly.end).toBe("2020-12-31 23:59:59");
  });
  //TODO: To create getDateRange tests for other cases
  it("Date range test weekly and calendar view - start date", () => {
    expect(dateRangeWeekly.start).toBe("2020-03-30 00:00:00");
  });
  it("Date range test weekly and calendar view - end date", () => {
    expect(dateRangeWeekly.end).toBe("2020-05-03 23:59:59");
  });
  it("Date range test daily view - start date", () => {
    expect(dateRangeDaily.start).toBe("2020-04-01 00:00:00");
  });
  it("Date range test daily view - end date", () => {
    expect(dateRangeDaily.end).toBe("2020-04-30 23:59:59");
  });
});
