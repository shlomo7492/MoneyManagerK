import moment from "moment";

//CHECKs if there is token in LS or cookie in future
//and initialize the state
export const isSessionAlive = () => {
  const token = localStorage.getItem("AccessToken");
  if (!token || token === "") {
    return false;
  }
  return true;
};

//DATE_RANGE UTILITY
//On Daily view page
const getDailyDateRange = (selectDate) => {
  const startOfMonth = moment(selectDate)
    .startOf("month")
    .format("YYYY-MM-DD HH:mm:ss");
  const endOfMonth = moment(selectDate)
    .endOf("month")
    .format("YYYY-MM-DD HH:mm:ss");
  return { start: startOfMonth, end: endOfMonth };
};
//On Weekly and Calendar view pages
const getWeeklyDateRange = (selectDate) => {
  const startOfMonth = moment(selectDate).startOf("month").day();
  const endOfMonth = moment(selectDate).endOf("month").day();

  const daysToSubtract =
    startOfMonth === 0 ? 6 : startOfMonth === 1 ? 0 : startOfMonth - 1;
  const daysToAdd = endOfMonth === 0 ? 0 : 7 - endOfMonth;

  return {
    start: moment(selectDate)
      .startOf("month")
      .subtract(daysToSubtract, "d")
      .format("YYYY-MM-DD HH:mm:ss"),
    end: moment(selectDate)
      .endOf("month")
      .add(daysToAdd, "d")
      .format("YYYY-MM-DD HH:mm:ss"),
  };
};
//On Monthly view page
const getMonthlyDateRange = (selectDate) => {
  return {
    start: moment(selectDate.getFullYear().toString())
      .startOf("year")
      .format("YYYY-MM-DD HH:mm:ss"),
    end: moment(selectDate.getFullYear().toString())
      .endOf("year")
      .format("YYYY-MM-DD HH:mm:ss"),
  };
};
//DateRange export
export const getDateRange = (page, selectDate) => {
  switch (page) {
    case "daily":
      return getDailyDateRange(selectDate);
    case "weekly":
    case "calendar":
      return getWeeklyDateRange(selectDate);

    case "monthly": {
      return getMonthlyDateRange(selectDate);
    }
    default: {
      return getDailyDateRange(selectDate);
    }
  }
};

//Data formatters
//Monthly view data formatter
export const getFormattedDataMonthly = (finEvent, selectDate) => {
  const dateRange = getDateRange("monthly", selectDate);
  const formattedData = [];
  let monthStart = dateRange.start;
  let nextMonthStart = moment(monthStart)
    .add(1, "month")
    .format("YYYY-MM-DD 00:00:00");
  let monthEnd = moment(nextMonthStart)
    .subtract(1, "day")
    .format("YYYY-MM-DD 23:59:59");
  while (monthEnd < dateRange.end) {
    let incomeSummary = 0,
      expensesSummary = 0,
      currentMonth = false;

    // eslint-disable-next-line array-callback-return
    // eslint-disable-next-line no-loop-func
    finEvent.map((finE) => {
      if (finE.date < monthEnd && finE.date > monthStart) {
        if (finE.type === "income") {
          incomeSummary += finE.amount;
        } else {
          expensesSummary += finE.amount;
        }
      }
    });
    const today = moment().format("YYYY-MM-DD HH:mm:ss");
    if (today < monthEnd && today > monthStart) {
      currentMonth = true;
    }
    formattedData.push({
      startOfMonth: monthStart,
      endOfMonth: monthEnd,
      isCurrentMonth: currentMonth,
      monthLabel: moment(monthStart).format("MMM"),
      incomeSummary: incomeSummary,
      expensesSummary: expensesSummary,
    });

    monthStart = moment(monthEnd).add(1, "day").format("YYYY-MM-DD 00:00:00");
    monthEnd = moment(monthStart).endOf("month").format("YYYY-MM-DD 00:00:00");
  }
  return formattedData;
};
//Calendar view data formatter
export const getFormattedDataCalendar = (finEvent, selectDate) => {
  const dateRange = getDateRange("calendar", selectDate);
  const formattedData = [];

  let calendarCurrentTempDate = dateRange.start;
  while (calendarCurrentTempDate <= dateRange.end) {
    const calendarTempArrayItem = {};
    if (
      new Date(calendarCurrentTempDate).getTime() <
        new Date(moment(selectDate).startOf("month")).getTime() ||
      new Date(calendarCurrentTempDate).getTime() >
        new Date(moment(selectDate).endOf("month")).getTime()
    ) {
      calendarTempArrayItem.thisMonth = false;
    } else {
      calendarTempArrayItem.thisMonth = true;
    }
    if (calendarCurrentTempDate !== dateRange.end)
      calendarCurrentTempDate = moment(calendarCurrentTempDate).format(
        "YYYY-MM-DD 00:00:00"
      );
    calendarTempArrayItem.date = calendarCurrentTempDate;
    calendarTempArrayItem.dayLabel = moment(calendarCurrentTempDate).format(
      "D"
    );
    calendarTempArrayItem.incomeSummary = 0;
    calendarTempArrayItem.expensesSummary = 0;
    // eslint-disable-next-line no-loop-func
    finEvent.map((finE) => {
      if (
        moment(calendarCurrentTempDate).format("D") ===
          moment(finE.date).format("D") &&
        moment(calendarCurrentTempDate).format("M") ===
          moment(finE.date).format("M")
      ) {
        if (finE.type === "income") {
          calendarTempArrayItem.incomeSummary += finE.amount;
        } else {
          calendarTempArrayItem.expensesSummary += finE.amount;
        }
      }
    });

    formattedData.push(calendarTempArrayItem);
    calendarCurrentTempDate = moment(calendarCurrentTempDate)
      .add(1, "days")
      .format("YYYY-MM-DD 23:59:59");
  }

  return formattedData;
};
//Weekly view data formatter
export const getFormattedDataWeekly = (finEvent, selectDate, order) => {
  const dateRange = getDateRange("weekly", selectDate);
  //TODO: format data in [{week:"DD.MM.-DD.MM",incomeSummary:number,expenseSummary:number}]
  //TODO: create array of objects whit properties starOfTheWeek,
  //endOfTheWeek, weekDateRange etc. map trough it and then map trough the finEvent
  //push into formattedData {weekDateRange,incomeSum,expensesSum}
  const formattedData = [];
  let weekStart = dateRange.start;
  let weekEnd = weekStart;
  while (weekEnd < dateRange.end) {
    weekEnd = moment(weekStart).add(6, "days").format("YYYY-MM-DD 23:59:59");
    let incomeSummary = 0,
      expensesSummary = 0,
      currentWeek = false;
    // eslint-disable-next-line array-callback-return
    // eslint-disable-next-line no-loop-func
    finEvent.map((finE) => {
      if (
        new Date(finE.date).getTime() > new Date(weekStart).getTime() &&
        new Date(finE.date).getTime() < new Date(weekEnd).getTime()
      ) {
        if (finE.type === "income") {
          incomeSummary += finE.amount;
        } else {
          expensesSummary += finE.amount;
        }
      }
    });
    if (
      new Date(moment().format("YYYY-MM-DD 00:00:00")).getTime() <=
        new Date(weekEnd).getTime() &&
      new Date(moment().format("YYYY-MM-DD 00:00:00")).getTime() >=
        new Date(weekStart).getTime()
    ) {
      currentWeek = true;
    }
    formattedData.push({
      startOfWeek: weekStart,
      endOfWeek: weekEnd,
      isCurrentWeek: currentWeek,
      weekLabel:
        moment(weekStart).format("DD.MM") +
        " ~ " +
        moment(weekEnd).format("DD.MM"),
      incomeSummary: incomeSummary,
      expensesSummary: expensesSummary,
    });
    weekStart = moment(weekEnd).add(1, "days").format("YYYY-MM-DD 00:00:00");
  }

  return order === "desc"
    ? formattedData.sort((a, b) => (a.startOfWeek > b.startOfWeek ? -1 : 1))
    : formattedData.sort((a, b) => (a.startOfWeek < b.startOfWeek ? -1 : 1));
};
// Total view: compare prev month and this month expenses %
export const compareExpensesIncreaseWithPrevMonth = (prevMonth, thisMonth) => {
  let thisMonthExpenses = 0;
  thisMonth.map((finE) => {
    if (finE.type === "expense") {
      thisMonthExpenses += finE.amount;
    }
  });
  return prevMonth !== 0
    ? thisMonthExpenses >= prevMonth
      ? Math.round(thisMonthExpenses / (prevMonth / 100))
      : 0
    : thisMonthExpenses === 0
    ? 0
    : 100;
};
//This will edit the date, to show only data for the
// current month in to the summary layer
export const getCalendarSummaryData = (selectDate, finEventData) => {
  const presentMonthInNumber = moment(selectDate).format("M");
  const finEventExportData = [];
  // eslint-disable-next-line array-callback-return
  finEventData.map((finE) => {
    if (presentMonthInNumber === moment(finE.date).format("M")) {
      finEventExportData.push(finE);
    }
  });
  return finEventExportData;
};
