import apiCall from "../apiCall";
import { logOut } from "../../redux/actions/appActions";

const isAuthenticated = (res, dispatch) => {
  if (res.statusCode === 401) {
    dispatch(logOut());
  } else {
    return res;
  }
};

const getAllCategories = (token) => {
  return apiCall(
    {
      url: `categories?type=all`,
      method: "GET",
    },
    token
  );
};
const getCategoriesByType = (type, token) => {
  return apiCall(
    {
      url: `categories?type=${type}`,
      method: "GET",
    },
    token
  );
};
export const getCategory = (options, dispatch) => {
  let fetchedCategories;
  if (options.type && options.type !== "all") {
    fetchedCategories = getCategoriesByType(options.type, options.token);
  } else {
    fetchedCategories = getAllCategories(options.token);
  }
  return fetchedCategories.then(async (res) => {
    return isAuthenticated(res, dispatch);
  });
};

/* CREATE CATEGORY */
export const createNewCategory = (options, dispatch) => {
  return apiCall(
    {
      url: `categories`,
      method: "POST",
      data: {
        type: options.type,
        name: options.name,
        icon: options.icon,
      },
    },
    options.token
  ).then(async (res) => {
    return isAuthenticated(res, dispatch);
  });
};

/* UPDATE/EDIT CATEGORY */
export const editCategory = (options, dispatch) => {
  const bodyData = {};
  if (options.type) bodyData.type = options.type;
  if (options.name) bodyData.name = options.name;
  if (options.icon) bodyData.icon = options.icon;
  return apiCall(
    {
      url: `categories/${options.id}`,
      method: "PATCH",
      data: bodyData,
    },
    options.token
  ).then(async (res) => {
    return isAuthenticated(res, dispatch);
  });
};

/* DELETE CATEGORY */
export const deleteOneCategory = (options, dispatch) => {
  return apiCall(
    {
      url: `categories/${options.id}`,
      method: "DELETE",
    },
    options.token
  ).then(async (res) => {
    return isAuthenticated(res, dispatch);
  });
};
