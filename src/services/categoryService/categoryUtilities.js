import { List } from "immutable";

//ADD_CATEGORY
export const addCategories = (state, action) => {
  if (action.data) {
    localStorage.setItem("AccessToken", action.data.accessToken);
    return state.setIn(["category"], List(action.data.data));
  } else {
    return state;
  }
};

//CREATE_CATEGORY
export const createCategory = (state, action) => {
  const currentCategories = state.getIn(["category"]);
  return state
    .setIn(["category"], List([...currentCategories]).push(action.data.data))
    .setIn(["currentCategory"], action.data.data._id);
};

//CHANGE_CURRENT_CATEGORY
export const changeCurrentCategory = (state, data) => {
  return state.setIn(["currentCategory"], data);
};

//REMOVE_CATEGORY_FROM_STATE:
export const clearCategoryFromState = (state) => {
  return state.setIn(["category"], List()).setIn(["currentCategory"], "");
};
