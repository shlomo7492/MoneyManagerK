import fetchCall from "./fetch/fetchCall";
import { axiosCall } from "./axios/axiosCall";

async function apiCall(options, token) {
  if ("fetch" in window) {
    return fetchCall(options, token);
  } else {
    //implement axios actions
    return axiosCall(options, token);
  }
}
export default apiCall;
