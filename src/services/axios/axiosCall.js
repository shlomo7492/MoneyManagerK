import Api from "./axios";

const successCodes = [200, 201, 304];

export const axiosCall = (options, token) => {
  const myRequest = {
    ...options,
    headers: {
      "Content-Type": "application/json",
      Authorization: "Bearer " + token
    }
  };

  return Api(myRequest.url, myRequest)
    .then(res => {
      if (successCodes.includes(res.status)) {
        return res.data;
      } else {
        return { statusCode: res.status, statusText: res.statusText };
      }
    })
    .catch(err => {
      console.log(err.message);
    });
};

//TO DO:
// create class and GET POST PUT DELETE methods
