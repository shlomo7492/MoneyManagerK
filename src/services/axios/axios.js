import axios from "axios";
//export const baseUrl = `http://localhost:3004/`;
export const baseUrl = `http://127.0.0.1:3001/`;

const axiosInstance = axios.create({
  baseURL: baseUrl
});

export default axiosInstance;
