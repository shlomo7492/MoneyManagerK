import apiCall from "../apiCall";
import { logOut } from "../../redux/actions/appActions";

const isAuthenticated = (res, dispatch) => {
  if (res.statusCode === 401) {
    dispatch(logOut());
  } else {
    return res;
  }
};

/* GET USER aka GET USER DATA*/
export const getUserData = (options, dispatch) => {
  return apiCall(
    {
      url: `users`,
      method: "GET",
    },
    options.token
  ).then(async (res) => {
    return isAuthenticated(res, dispatch);
  });
};
/* CREATE USER aka REGISTER USER */
export const registerUser = (options) => {
  return apiCall({
    url: `users`,
    method: "POST",
    data: {
      name: options.name,
      email: options.email,
      password: options.password,
    },
  });
};

/* LOGIN USER */
export const userLogin = (options, dispatch) => {
  return apiCall({
    url: `users/login`,
    method: "POST",
    data: {
      email: options.email,
      password: options.password,
    },
  }).then(async (res) => {
    return isAuthenticated(res, dispatch);
  });
};

/* EDIT/UPDATE USER */
export const editUserData = (options, dispatch) => {
  const bodyData = {};
  if (options.email) bodyData.email = options.email;
  if (options.name) bodyData.name = options.name;
  if (options.password) bodyData.password = options.password;
  if (options.newPassword) bodyData.newPassword = options.newPassword;
  return apiCall(
    {
      url: `users/${options.userId}`,
      method: "PATCH",
      data: bodyData,
    },
    options.token
  ).then(async (res) => {
    return isAuthenticated(res, dispatch);
  });
};

/* DELETE USER */
export const deleteUserData = (options, dispatch) => {
  return apiCall({
    url: `users/${options.userId}`,
    method: "DELETE",
  }).then(async (res) => {
    return isAuthenticated(res, dispatch);
  });
};
