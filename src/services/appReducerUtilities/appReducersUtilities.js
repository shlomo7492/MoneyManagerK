import { Map } from "immutable";
import moment from "moment";
// handle changes for selectDate
export const changeSelectDate = (state, action) => {
  return state
    .setIn(["selectDate"], action.data.selectDate)
    .setIn(["dateType"], action.data.dateType);
};
//Update Current Balance
export const updateCurrentBalance = (state, action) => {
  let income = 0,
    expense = 0,
    balance = 0;
  if (action.data) {
    // eslint-disable-next-line array-callback-return
    action.data.finEvent.map((event) => {
      if (event.type === "income") {
        income += event.amount;
      } else {
        expense += event.amount;
      }
    });
    balance = income - expense;
  }
  return state.setIn(
    ["finEventSummary"],
    Map({ income: income, expense: expense, balance: balance })
  );
};

//Modal managing function NO MORE IN USE
// export const manageModalShow = (state, action) => {
//   return state.setIn(["modal"], fromJS(action.data));
// };

// On login function
export const onLoginAction = (state, action) => {
  const res = action.data;
  if (res && res.statusCode !== 401) {
    localStorage.setItem("AccessToken", res.accessToken);
    return state.setIn(["loggedIn"], true).setIn(["user"], Map(res.data));
  }
  return state;
};

//Getting user from DB by calling action getUser and setting the state
export const setUser = (state, action) => {
  localStorage.setItem("AccessToken", action.data.accessToken);
  return state.setIn(["user"], Map(action.data.data));
};
//OnLogOut function
export const onLogOut = (state, action) => {
  //localStorage.setItem("AccessToken", "");
  localStorage.clear();

  return state
    .setIn(["selectDate"], new Date(moment().format("YYYY-MM-DD")))
    .setIn(["loggedIn"], false)
    .setIn(["user"], Map({}));
};
//OnClearSummary function
export const onClearCurrentBalance = (state) => {
  return state.setIn(
    ["finEventSummary"],
    Map({ income: 0, expense: 0, balance: 0 })
  );
};

//onManageIsFormState function
export const onManageIsFormState = (state, action) => {
  return state.setIn(["isForm"], action.data);
};
