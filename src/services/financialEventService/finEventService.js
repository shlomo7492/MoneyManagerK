import apiCall from "../apiCall";
import { logOut } from "../../redux/actions/appActions";

/*NOT AUTHENTICATED HANDLER FUNCTION */

const isAuthenticated = (res, dispatch) => {
  if (res.statusCode === 401) {
    dispatch(logOut());
  } else {
    return res;
  }
};

/* GET METHODS */
const getAllFinEventsByType = (type, token) => {
  //TODO: Here is the ApiCall for getting all events
  //of specific type income/expense
  return apiCall(
    {
      url: `finevent?category=${type}`,
      method: "GET",
    },
    token
  );
};
const getAllFinEvents = (token) => {
  //TODO: Here is the ApiCall for getting all events
  //regardless of type or date range
  return apiCall(
    {
      url: `finevent`,
      method: "GET",
    },
    token
  );
};
const getAllFinEventsByDateRange = (start, end, token) => {
  //TODO: ApiCall for allEvents that have date
  //between start and end date regardless of type
  //Returns Promise to be used in the action service functions
  return apiCall(
    {
      url: `finevent?start=${start}&end=${end}&category=all`,
      method: "GET",
    },
    token
  );
};
const getAllFinEventsByDateRangeAndType = (start, end, type, token) => {
  //TODO: ApiCall for allEvents that have date
  //between start and end date and
  //of specific type income/expense
  return apiCall(
    {
      url: `finevent?start=${start}&end=${end}&category=${type}`,
      method: "GET",
    },
    token
  );
};
const getOneFinEvent = (id, token) => {
  //TODO: Here is the ApiCall for getting one event
  //with specific id
  return apiCall(
    {
      url: `finevent/${id}`,
      method: "GET",
    },
    token
  );
};

/* MAIN GET METHOD THAT CALLS THE OTHERS */

export const getFinEvent = (options, dispatch) => {
  let fetchedData;
  if (options.id) {
    fetchedData = getOneFinEvent(options.id, options.token);
  } else {
    if (!options.start || !options.end) {
      if (!options.type) {
        fetchedData = getAllFinEvents(options.token);
      } else {
        fetchedData = getAllFinEventsByType(options.type, options.token);
      }
    } else {
      if (!options.type) {
        fetchedData = getAllFinEventsByDateRange(
          options.start,
          options.end,
          options.token
        );
      } else {
        fetchedData = getAllFinEventsByDateRangeAndType(
          options.start,
          options.end,
          options.type,
          options.token
        );
      }
    }
  }
  return fetchedData.then(async (res) => {
    return isAuthenticated(res, dispatch);
  });
};

/*CREATE NEW FINANCIAL EVENT */
export const createFinEvent = (options, dispatch) => {
  return apiCall(
    {
      url: `finevent`,
      method: "POST",
      data: {
        type: options.type,
        amount: options.amount,
        date: options.date,
        description: options.description,
        category: options.category,
      },
    },
    options.token
  ).then(async (res) => {
    return isAuthenticated(res, dispatch);
  });
};

export const updateFinEvent = (options, dispatch) => {
  const bodyData = {};
  if (options.type) bodyData.type = options.type;
  if (options.amount) bodyData.amount = options.amount;
  if (options.date) bodyData.date = options.date;
  if (options.description) bodyData.description = options.description;
  if (options.category) bodyData.category = options.category;
  return apiCall(
    {
      url: `finevent/${options.id}`,
      method: "PATCH",
      data: bodyData,
    },
    options.token
  ).then(async (res) => {
    return isAuthenticated(res, dispatch);
  });
};

export const deleteFinEvent = (options, dispatch) => {
  return apiCall(
    {
      url: `finevent/${options.id}`,
      method: "DELETE",
    },
    options.token
  ).then(async (res) => {
    return isAuthenticated(res, dispatch);
  });
};
