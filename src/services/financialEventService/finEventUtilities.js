import { List } from "immutable";

//ADD_FIN_EVENT
export const addFinEvent = (action, state) => {
  if (action.data) {
    localStorage.setItem("AccessToken", action.data.accessToken);
    return state.setIn(["finEvent"], List([...action.data.data]));
  } else return state;
};
//ADD_PREV_MONTH_SUMMARY_BY_TYPE
export const addPrevMonthSummaryByType = (action, state) => {
  let amount = 0;
  if (action.data) {
    action.data.data.map((finE) => (amount += finE.amount));
    return state.setIn(["prevMonthExpenses"], amount);
  } else {
    return state;
  }
};
//REMOVE_FIN_EVENT_FROM_STATE
export const removeFinEventFromState = (state) => {
  return state.setIn(["finEvent"], List([]));
};
