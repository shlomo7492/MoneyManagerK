import { baseUrl } from "../axios/axios";
const successCodes = [200, 201, 304];

const fetchCall = async (options, token) => {
  const myRequest = {
    ...options,
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
      Authorization: "Bearer " + token
    }
  };
  let result;
  switch (options.method) {
    case "POST":
    case "PUT":
    case "PATCH":
      result = fetchCreateOrUpdate(myRequest);
      break;
    case "DELETE":
      result = fetchDelete(myRequest);
      break;
    default:
      result = fetchGet(myRequest);
      break;
  }
  return result;
};

//GET
const fetchGet = async options => {
  return await fetch(baseUrl + options.url, {
    method: "GET",
    headers: options.headers,
    credentials: options.credentials
  })
    .then(res => {
      if (successCodes.includes(res.status)) {
        return res.json();
      } else {
        return { statusCode: res.status, statusText: res.statusText };
      }
    })
    .catch(err => {});
};
//POST And PUT
const fetchCreateOrUpdate = async options => {
  return await fetch(baseUrl + options.url, {
    method: options.method,
    //cache: "no-cache",
    headers: options.headers,
    credentials: options.credentials,
    //credentials: "same-origin",
    body: JSON.stringify(options.data)
  })
    .then(res => {
      if (successCodes.includes(res.status)) {
        return res.json();
      } else {
        return {
          statusCode: res.status,
          statusText: res.statusText
        };
      }
    })
    .catch(err => {});
};

//DELETE
const fetchDelete = async options => {
  return await fetch(baseUrl + options.url, {
    method: options.method,
    headers: options.headers
  })
    .then(res => {
      if (successCodes.includes(res.status)) {
        return res.json();
      } else {
        return { statusCode: res.status, statusText: res.statusText };
      }
    })
    .catch(err => {});
};
// To create  and DELETE methods

export default fetchCall;
