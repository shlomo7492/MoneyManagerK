import React from "react";
//import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Route, Switch, withRouter } from "react-router-dom";
import Daily from "./components/pages/Daily/Daily";
import Calendar from "./components/pages/Calendar/Calendar";
import Weekly from "./components/pages/Weekly/Weekly";
import Monthly from "./components/pages/Monthly/Monthly";
import Total from "./components/pages/Total/Total";
import User from "./components/pages/User/User";
import MainLayout from "./components/MainLayout/MainLayout";
import LoginForm from "./containers/UserForm/LoginForm";
import RegistrationForm from "./containers/UserForm/RegistrationForm";
import Logout from "./containers/Logout/Logout";
import AddEditFinancialEventForm from "./containers/IncomeExpenseForm/IncomeExpenseForm";
// MODAL NO MORE IN USE
//import GlobalModalWrapper from "./containers/GlobalModalWrapper/GlobalModalWrapper";
import "../node_modules/bootstrap/dist/css/bootstrap.min.css";
import "./App.css";

const App = (props) => {
  return (
    <Switch>
      <Route path="/login" component={LoginForm} />
      <Route path="/register" component={RegistrationForm} />
      <Route path="/logout" component={Logout} />
      <MainLayout {...props}>
        <Switch>
          <Route
            path="/financialEvent/:action/:param"
            exact
            component={AddEditFinancialEventForm}
          />
          <Route path="/daily/:selectDate/:order" exact component={Daily} />
          <Route path="/weekly/:selectDate/:order" component={Weekly} />
          <Route path="/daily/:selectDate" exact component={Daily} />
          <Route path="/calendar/:selectDate" component={Calendar} />
          <Route path="/weekly/:selectDate" component={Weekly} />
          <Route path="/monthly/:selectDate" component={Monthly} />
          <Route path="/total/:selectDate" component={Total} />
          <Route path="/calendar" component={Calendar} />
          <Route path="/weekly" component={Weekly} />
          <Route path="/monthly" component={Monthly} />
          <Route path="/total" component={Total} />
          <Route path="/user" component={User} />
          <Route path="/" component={Daily} />
        </Switch>
        {/* //MODAL NO MORE IN USE
        <GlobalModalWrapper
          show={props.modal.getIn(["show"])}
          type={props.modal.getIn(["type"])}
        >*/}
      </MainLayout>
    </Switch>
  );
};

//MODAL NO MORE IN USE
// GlobalModalWrapper.propTypes = {
//   show: PropTypes.bool,
//   type: PropTypes.string
// };
const mapStateToProps = (state) => {
  return {
    //MODAL NO MORE IN USE
    //modal: state.appState.getIn(["modal"]),
    categories: state.categoryState.get("category"),
  };
};
export default connect(mapStateToProps, null)(withRouter(App));
