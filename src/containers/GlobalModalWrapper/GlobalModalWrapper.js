import React, { Component } from "react";
import { connect } from "react-redux";
import { modalManage } from "../../redux/actions/appActions"; //NO MORE IN USE PUTTED IN COMMENTS
import { Modal } from "react-bootstrap";
import IncomeExpenseForm from "../IncomeExpenseForm/IncomeExpenseForm";

class GlobalModalWrapper extends Component {
  showModal = (type) => {
    this.props.modalManage(true, type, this.props.modal.data);
  };

  closeModal = () => {
    this.props.modalManage(false, "", "");
  };
  render() {
    let selectContent, modalTitle;
    switch (this.props.modal.getIn(["type"])) {
      case "AppInfo":
        modalTitle = "Application Info";
        selectContent = (
          <>
            <h2>MONEY ManagerK v 1.0</h2>
            <p>This is a test project for Kodar Ltd.</p>
            <p>
              Created by Dimitar Daskalov under supervision of Kliment Mirchev
            </p>
            <p>November/December 2019</p>
          </>
        );
        break;
      case "AddFinancialEvent":
        modalTitle = "Add Income/Expense:";
        selectContent = (
          <>
            <IncomeExpenseForm
              formType="create"
              data={this.props.modal.getIn(["data"]).toJS()}
              onclick={this.closeModal}
            />
          </>
        );
        break;
      case "EditFinancialEvent":
        modalTitle = "Edit Income/Expense:";
        selectContent = (
          <>
            <IncomeExpenseForm
              formType="edit"
              data={this.props.modal.toJS().data}
              onclick={this.closeModal}
            />
          </>
        );
        break;

      default:
        break;
    }
    return (
      <>
        <Modal
          size="lg"
          show={this.props.modal.getIn(["show"])}
          centered={true}
          backdrop={true}
          onHide={this.closeModal}
        >
          <Modal.Header closeButton>
            <Modal.Title>{modalTitle}</Modal.Title>
          </Modal.Header>
          <Modal.Body>{selectContent}</Modal.Body>
        </Modal>
      </>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    category: state.categoryState.get("category"),
    modal: state.appState.getIn(["modal"]),
  };
};
const mapDispatchToProps = {
  modalManage: modalManage,
};
export default connect(mapStateToProps, mapDispatchToProps)(GlobalModalWrapper);
