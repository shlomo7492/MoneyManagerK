import React, { Component } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCaretDown } from "@fortawesome/free-solid-svg-icons";
import classes from "./Collapsible.module.css";
class Collapsible extends Component {
  state = {
    open: false
  };
  onCollappsibleButtonToggle = event => {
    this.setState({ open: !this.state.open });
  };
  render() {
    const collapsibleClass = this.state.open
      ? classes.Collapsible + " " + classes.Open
      : classes.Collapsible;
    const caretArrowStyle = this.state.open
      ? classes.CaretArrow + " " + classes.CaretOpen
      : classes.CaretArrow;
    const errorClass =
      this.props.errorMsg && this.props.errorMsg.length > 0
        ? "w-75 ml-4 invalid-feedback d-block"
        : "text-secondary  w-75 ml-4 ";
    return (
      <div>
        <div className="d-flex flex-row justify-content-between h-50">
          <div className="d-flex flex-row justify-content-between">
            <div>{this.props.title}</div>
            <div className={errorClass}>
              {this.props.errorMsg.length > 0
                ? this.props.errorMsg
                : this.props.categoryName.length > 0
                ? this.props.categoryName
                : " "}
            </div>
          </div>
          <div onClick={this.onCollappsibleButtonToggle} className="">
            <div className={classes.ToggleButtonContainer}>
              <FontAwesomeIcon icon={faCaretDown} className={caretArrowStyle} />
            </div>
          </div>
        </div>
        <div className={collapsibleClass + " mt-2"}>{this.props.children}</div>
      </div>
    );
  }
}
export default Collapsible;
