import React, { Component } from "react";
import { Spinner } from "react-bootstrap";
import classes from "./Logout.module.css";
class Logout extends Component {
  componentDidMount() {
    setTimeout(this.props.history.push("/login"), 2000);
  }
  render() {
    return (
      <div className={classes.Logout}>
        <Spinner animation="border" size="md"></Spinner>
        <span> Logging you out...</span>
      </div>
    );
  }
}
export default Logout;
