//To open in new modal. handles writing to DB
import React, { Component } from "react";
import { connect } from "react-redux";
import moment from "moment";
import { isSessionAlive } from "../../services/utilityService/utilityService";
import {
  selectDateChange,
  manageIsFormState,
} from "../../redux/actions/appActions";
import {
  addFinEvent,
  updateOneFinEvent,
  createNewFinEvent,
} from "../../redux/actions/finEventActions";
import {
  addCategory,
  createCategory,
  changeCurrentCategory,
} from "../../redux/actions/categoryActions";

import { Form, Button, FormCheck, Row, Col } from "react-bootstrap";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import Categories from "../../components/Categories/Categories";
import classes from "./IncomeExpenseForm.module.css";
const isFormValid = (state, category) => {
  console.log(state, category);
  let valid = true;
  const errors = state.errors;
  for (var key in state.errors) {
    if (errors[key].length > 0) valid = false;
  }
  if (!state.amount.length > 0) valid = false;
  console.log(valid);
  if (!category.length > 0) valid = false;
  if (!state.description.length > 0) valid = false;
  return valid;
};
class IncomeExpenseForm extends Component {
  state = {
    edit: true,
    eventId: "",
    expense: true,
    date: "",
    amount: "",
    description: "",
    initialDate: "",
    openCategories: false,
    errors: {
      amount: "",
      description: "",
      category: "",
    },
  };
  radioButtonHandler = (event) => {
    const expense = event.target.id === "inc" ? false : true;
    this.setState({ expense: expense });
  };
  onChangeHandler = (event) => {
    const tempState = this.state;

    switch (event.target.name) {
      case "amount":
        tempState.amount = event.target.value;
        break;
      case "category":
        this.props.changeCurrentCategory(event.target.id);
        break;
      case "description":
        tempState.description = event.target.value;
        break;
      default:
        break;
    }
    tempState.errors.amount =
      tempState.amount === "" || parseFloat(tempState.amount) <= 0
        ? "Amount is required and must be positive number"
        : "";
    tempState.errors.description =
      tempState.description.trim().length > 0 ? "" : "Description is required.";
    tempState.errors.category =
      this.props.currentCategory.length > 0
        ? ""
        : "Category is required.Please select one";
    this.setState({ state: tempState });
  };
  onDateChangeHandler = (date) => {
    console.log(43, date);
    if (this.props.match.params.action !== "edit") {
      this.props.history.push(
        `/financialEvent/new/${moment(date).format("YYYY-MM-DD")}`
      );
    } else {
      this.setState({ date });
    }
  };
  setCategoriesToggle = () => {
    this.setState({ openCategories: !this.state.openCategories });
  };
  onSubmitHandler = (event) => {
    event.preventDefault();
    //TODO:
    //POST data and then redirect to daily view
    // with current date in url param
    console.log(
      this.state,
      this.props.currentCategory,
      isFormValid(this.state, this.props.currentCategory)
    );
    if (isFormValid(this.state, this.props.currentCategory)) {
      if (this.props.match.params.action === "edit") {
        this.updateFinEvent().then((res) => {
          console.log("I am here UPDATE");
        });
      } else {
        this.postFinEvent().then((res) => {
          console.log("I am here POST");
        });
      }
      this.props.history.push(
        `/daily/${moment(this.state.date).format("YYYY-MM-DD")}`
      );
    }
  };
  onCancelHandler = () => {
    this.props.history.push(
      `/daily/${moment(this.state.initialDate).format("YYYY-MM-DD")}`
    );
  };
  getFinEvent = async () => {
    const finEvent = await this.props.finEvent;
    if (finEvent.size === 1) {
      console.log(74, finEvent, this.props.finEvent.size);
      let date, type, amount, description, category;
      finEvent.map((finE) => {
        date = new Date(finE.date);
        type = finE.type === "expense";
        amount = finE.amount.toString();
        description = finE.description;
        category = finE.category;
      });

      this.setState({
        edit: false,
        expense: type,
        date: date,
        amount: amount,
        description: description,
        initialDate: date,
      });
      this.props.changeCurrentCategory(category);
      this.props.selectDateChange(date, "month");
    }
  };
  postFinEvent = async () => {
    this.props.createNewFinEvent({
      type: this.state.expense ? "expense" : "income",
      date: this.state.date,
      amount: this.state.amount,
      category: this.props.currentCategory,
      description: this.state.description,
      token: this.state.token,
    });
  };
  updateFinEvent = async () => {
    this.props.updateOneFinEvent({
      id: this.state.eventId,
      type: this.state.expense ? "expense" : "income",
      date: this.state.date,
      amount: this.state.amount,
      category: this.state.category,
      description: this.state.description,
      token: this.state.token,
    });
  };

  onAddCategoryHandler = (data) => {
    console.log(
      "Logged in:",
      typeof this.props.loggedIn,
      this.props.loggedIn,
      "isSessionAlive:",
      isSessionAlive()
    );
    const token = localStorage.getItem("AccessToken");
    this.props.createCategory({
      name: data.name,
      type: data.type,
      icon: data.icon,
      token: token,
    });
  };
  componentDidUpdate = (prevProps, prevState) => {
    const token = localStorage.getItem("AccessToken");

    if (prevProps.currentCategory !== this.props.currentCategory) {
      const errorsState = this.state.errors;
      errorsState.category =
        this.props.currentCategory.length > 0
          ? ""
          : "Category is required.Please select one";
      this.setState({
        errors: errorsState,
      });
    }
    if (
      prevProps.match.params.action === this.props.match.params.action &&
      this.props.match.params.action !== "edit"
    ) {
      if (prevProps.match.params.param !== this.props.match.params.param) {
        const date = new Date(this.props.match.params.param);
        this.props.selectDateChange(date, "month");
        this.setState({ date: date });
      }
    } else {
      if (prevProps.match.params.param !== this.props.match.params.param) {
        this.setState({ eventId: this.props.match.params.param, edit: true });
      } else if (this.state.edit === true) {
        this.getFinEvent();
      }
    }
    if (this.props.categories.length === 0) {
      this.props.addCategory({ token: token });
    }
  };
  componentDidMount = () => {
    const token = localStorage.getItem("AccessToken");
    const parameter = this.props.match.params.param;
    if (typeof this.props.categories.length === "undefined") {
      this.props.addCategory({ token: token });
    }
    if (this.props.match.params.action === "edit") {
      this.props.addFinEvent({
        id: parameter,
        token: token,
      });
      this.setState({ eventId: parameter, token: token });
    } else {
      const date = new Date(parameter);
      this.setState({
        date: date,
        initialDate: date,
        edit: true,
        token: token,
      });
      this.props.selectDateChange(date, "month");
    }
    this.props.manageIsFormState(true);
  };
  render() {
    console.log(
      213,
      this.state,
      this.props.categories,
      this.props.match.params
    );

    const classExpense = this.state.expense
      ? classes.RadioButton + " " + classes.CheckedRadioButton
      : classes.RadioButton;
    const classIncome = !this.state.expense
      ? classes.RadioButton + " " + classes.CheckedRadioButton
      : classes.RadioButton;
    const categoriesArray = [];
    return (
      <>
        <div className={classes.FormContainer}>
          <Row className="w-100 mx-auto px-0">
            <Col>
              <h3 className="p-3 pt-5">Create new financial event:</h3>
              <Form
                onSubmit={(event) => this.onSubmitHandler(event)}
                className="m-3"
              >
                <Form.Group className="d-flex justify-content-start">
                  <Form.Label className="mr-3">Type:</Form.Label>
                  <div className="d-flex justify-content-between w-25">
                    <FormCheck bsPrefix={classIncome}>
                      <FormCheck.Input
                        type={"radio"}
                        name="type"
                        id="inc"
                        checked={!this.state.expense}
                        onChange={this.radioButtonHandler}
                      />
                      <FormCheck.Label htmlFor="inc">Income</FormCheck.Label>
                    </FormCheck>
                    <FormCheck bsPrefix={classExpense}>
                      <FormCheck.Input
                        type={"radio"}
                        name="type"
                        id="exp"
                        checked={this.state.expense}
                        onChange={this.radioButtonHandler}
                      />
                      <FormCheck.Label htmlFor="exp">Expense</FormCheck.Label>
                    </FormCheck>
                  </div>
                </Form.Group>
                <Form.Group>
                  <div className="d-flex justify-content-end w-100">
                    <Form.Label className={classes.Label}>Amount:</Form.Label>
                    <Form.Control
                      type="number"
                      name="amount"
                      min="0"
                      step="0.01"
                      value={this.state.amount}
                      onChange={this.onChangeHandler}
                      isInvalid={this.state.errors.amount.length > 0}
                      className={classes.Input}
                    ></Form.Control>
                  </div>
                  <div className="d-flex justify-content-end w-100">
                    <div className={classes.Label}> &nbsp; </div>
                    <div
                      className={"invalid-feedback d-block " + classes.Input}
                    >
                      {this.state.errors.amount}
                    </div>
                  </div>
                </Form.Group>
                <Form.Group className="d-flex justify-content-start">
                  <Form.Label className={classes.Label}>Date:</Form.Label>
                  <div className={classes.Calendar}>
                    <DatePicker
                      dateFormat="yyyy-MM-dd"
                      timeCaption="date"
                      selected={this.state.date}
                      onClick={(date) => this.handleClick(date)}
                      onChange={(date) => this.onDateChangeHandler(date)}
                      shouldCloseOnSelect={true}
                      className={classes.CalendarInput}
                      popperPlacement="middle-start"
                      popperModifiers={{
                        offset: {
                          enabled: true,
                          offset: "10px, 15px",
                        },
                        preventOverflow: {
                          enabled: true,
                          escapeWithReference: false, // force popper to stay in viewport (even when input is scrolled out of view)
                          boundariesElement: "viewport",
                        },
                      }}
                    />
                  </div>
                </Form.Group>

                {this.props.categories &&
                  // eslint-disable-next-line array-callback-return
                  this.props.categories.map((cat) => {
                    if ((cat.type === "expense") === this.state.expense) {
                      categoriesArray.push(cat);
                    }
                  })}
                <div>
                  <Categories
                    classes={classes}
                    currentCategory={this.props.currentCategory}
                    errorMsg={this.state.errors.category}
                    categoryType={this.state.expense ? "expense" : "income"}
                    categories={categoriesArray}
                    onclick={this.setCategoriesToggle}
                    openCategories={this.state.openCategories}
                    onchange={this.onChangeHandler}
                    onAddCategory={this.onAddCategoryHandler}
                  />
                </div>

                <Form.Group>
                  <div className="d-flex justify-content-end w-100">
                    <Form.Label className={classes.Label}>
                      Description:{" "}
                    </Form.Label>
                    <Form.Control
                      type="text"
                      name="description"
                      placeholder={this.props.type}
                      value={this.state.description}
                      onChange={this.onChangeHandler}
                      isInvalid={this.state.errors.description.length > 0}
                    ></Form.Control>
                  </div>{" "}
                  <div className="d-flex justify-content-end w-100">
                    <div className={classes.Label}> &nbsp; </div>
                    <div
                      className={"invalid-feedback d-block " + classes.Input}
                    >
                      {this.state.errors.description}
                    </div>
                  </div>
                </Form.Group>
                <div className={classes.ButtonWrapper}>
                  <Button
                    variant="primary"
                    className="mr-1"
                    type="submit"
                    disabled={
                      !isFormValid(this.state, this.props.currentCategory)
                    }
                  >
                    Submit
                  </Button>
                  <Button
                    variant="outline-secondary"
                    onClick={this.onCancelHandler}
                  >
                    Cancel
                  </Button>
                </div>
              </Form>
            </Col>
          </Row>
        </div>
      </>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    categories: state.categoryState.get("category"),
    currentCategory: state.categoryState.get("currentCategory"),
    finEvent: state.financialEventState.get("finEvent"),
    loggedIn: state.appState.get("loggedIn"),
  };
};
const mapDispatchToProps = {
  addCategory: addCategory,
  selectDateChange: selectDateChange,
  manageIsFormState: manageIsFormState,
  addFinEvent: addFinEvent,
  updateOneFinEvent: updateOneFinEvent,
  createNewFinEvent: createNewFinEvent,
  createCategory: createCategory,
  changeCurrentCategory: changeCurrentCategory,
};
export default connect(mapStateToProps, mapDispatchToProps)(IncomeExpenseForm);
