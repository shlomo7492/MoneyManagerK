import React, { Component } from "react";
import { NavLink, withRouter } from "react-router-dom";
import { Formik } from "formik";
import * as yup from "yup";
import { Form, Button, Row, Col, Navbar } from "react-bootstrap";
import { connect } from "react-redux";
import { logIn, clearDisplayBalance } from "../../redux/actions/appActions";
import { removeFinEventFromState } from "../../redux/actions/finEventActions";
import { removeCategoryFromState } from "../../redux/actions/categoryActions";
import Logo from "../../components/Navigation/Logo/Logo";
import classes from "./UserForm.module.css";
const schema = yup.object({
  email: yup
    .string()
    .email("Email, must be a valid email")
    .required("Email is required"),
  password: yup.string().required("Password is required"),
});
class LoginForm extends Component {
  onSubmitHandler = (values) => {
    this.props.logIn({ email: values.email, password: values.password });
  };
  componentDidUpdate = () => {
    if (this.props.loggedIn) {
      this.props.history.push("/");
    }
  };
  componentDidMount = () => {
    if (this.props.loggedIn) {
      this.props.history.push("/");
    } else {
      this.props.removeFinEventFromState();
      this.props.removeCategoryFromState();
      this.props.clearDisplayBalance();
    }
  };
  render() {
    const initialValues = {
      email: "",
      password: "",
    };
    return (
      <>
        <Formik
          validationSchema={schema}
          onSubmit={async (values, { resetForm }) => {
            this.onSubmitHandler(values);
            resetForm();
          }}
          initialValues={initialValues}
        >
          {({
            handleSubmit,
            handleChange,
            resetForm,
            handleBlur,
            values,
            touched,
            isValid,
            errors,
          }) => (
            <div className={classes.Container}>
              <Row className="w-100 mx-auto px-0">
                <Col className="w-100 mx-0 px-0">
                  <Navbar className={classes.FirstRow}>
                    <div>
                      <div>User Login</div>
                      <Navbar.Brand>
                        <Logo> Money Manager K</Logo>
                      </Navbar.Brand>
                    </div>
                  </Navbar>
                </Col>
              </Row>
              <Row className="w-100 mx-auto p-2 pt-4">
                <Col>
                  <Form noValidate onSubmit={handleSubmit} method="POST">
                    <Form.Group>
                      <Form.Label>Email address:</Form.Label>
                      <Form.Control
                        type="email"
                        name="email"
                        placeholder="Enter email"
                        value={values.email || ""}
                        onChange={handleChange}
                        isInvalid={errors.email}
                      />
                      <Form.Control.Feedback type="invalid">
                        {errors.email}
                      </Form.Control.Feedback>
                    </Form.Group>
                    <Form.Group>
                      <Form.Label>Password:</Form.Label>
                      <Form.Control
                        type="password"
                        name="password"
                        placeholder="Password"
                        value={values.password || ""}
                        onChange={handleChange}
                        isInvalid={errors.password}
                      />
                      <Form.Control.Feedback type="invalid">
                        {errors.password}
                      </Form.Control.Feedback>
                    </Form.Group>
                    <div className={classes.ButtonWrapper}>
                      <Button variant="primary" type="submit">
                        Login
                      </Button>
                      <Button variant="outline-danger" type="reset">
                        Reset
                      </Button>
                    </div>
                  </Form>
                </Col>
              </Row>
              <Row className="w-100 mx-auto pb-4">
                <Col>
                  <hr />
                  Do not have account? Click here to{" "}
                  <NavLink to="/register">Register</NavLink>.
                </Col>
              </Row>
            </div>
          )}
        </Formik>
      </>
    );
  }
}
const mapStateToProps = (state) => {
  return { loggedIn: state.appState.get("loggedIn") };
};

const mapDispatchToProps = {
  logIn: logIn,
  removeFinEventFromState: removeFinEventFromState,
  removeCategoryFromState: removeCategoryFromState,
  clearDisplayBalance: clearDisplayBalance,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(LoginForm));
