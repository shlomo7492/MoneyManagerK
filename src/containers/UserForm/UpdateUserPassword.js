import React, { Component } from "react";
import { Formik } from "formik";
import { Button, Form } from "react-bootstrap";
import * as yup from "yup";
import classes from "./UserForm.module.css";
const schema = yup.object({
  oldPassword: yup.string().required("Current Password is required"),
  newPassword: yup
    .string()
    .required("New Password is required")
    .matches(
      /^.*(?=.{8,})((?=.*[!@#$%^&*()\-_=+{};:,<.>]){1})(?=.*\d)(((?=.*[a-z]){1})((?=.*[A-Z]){1}).*)|((?=.*\d)((?=.*[а-я]){1})((?=.*[А-Я]){1}).*)$/,
      "Must Contain 8 Characters, One Uppercase, One Lowercase, One Number and one special case Character"
    ),
  confirmPassword: yup
    .string()
    .required("Please confirm your new password")
    .oneOf([yup.ref("newPassword"), null], "Passwords doesn't match"),
});
class UpdateUserPassword extends Component {
  render() {
    const initialValues = {
      oldPassword: "",
      newPassword: "",
      confirmPassword: "",
    };
    return (
      <>
        <Formik
          validationSchema={schema}
          onSubmit={async (values, { resetForm }) => {
            this.props.onSubmit({
              password: values.oldPassword,
              newPassword: values.newPassword,
            });
            resetForm();
          }}
          initialValues={initialValues}
        >
          {({
            handleSubmit,
            handleChange,
            resetForm,
            handleBlur,
            values,
            touched,
            isValid,
            errors,
          }) => (
            <div className={classes.UpdateUserPasswordFormContainer}>
              <Form noValidate onSubmit={handleSubmit}>
                <Form.Group controlId="oldPassword">
                  <Form.Control
                    size="md"
                    type="password"
                    name="oldPassword"
                    value={values.oldPassword || ""}
                    onChange={handleChange}
                    placeholder="Current Password"
                    isInvalid={errors.oldPassword}
                  />
                  <Form.Control.Feedback className="FeedBack" type="invalid">
                    {errors.oldPassword}
                  </Form.Control.Feedback>
                </Form.Group>
                <Form.Group controlId="newPassword">
                  <Form.Control
                    size="md"
                    type="password"
                    name="newPassword"
                    value={values.newPassword || ""}
                    onChange={handleChange}
                    placeholder="New Password"
                    isInvalid={errors.newPassword}
                  />
                  <Form.Control.Feedback className="FeedBack" type="invalid">
                    {errors.newPassword}
                  </Form.Control.Feedback>
                </Form.Group>
                <Form.Group controlId="formBasicConfirmPassword">
                  <Form.Control
                    size="md"
                    name="confirmPassword"
                    onChange={handleChange}
                    type="password"
                    value={values.confirmPassword || ""}
                    placeholder="Confirm New Password"
                    isInvalid={errors.confirmPassword}
                  />
                  <Form.Control.Feedback className="FeedBack" type="invalid">
                    {errors.confirmPassword}
                  </Form.Control.Feedback>
                </Form.Group>

                <div className="text-right">
                  <Button variant="primary" type="submit">
                    Change
                  </Button>
                  <Button
                    className="ml-1"
                    variant="outline-secondary"
                    onClick={this.props.onCancel}
                    type="cancel"
                  >
                    Cancel
                  </Button>
                </div>
              </Form>
            </div>
          )}
        </Formik>
      </>
    );
  }
}
export default UpdateUserPassword;
