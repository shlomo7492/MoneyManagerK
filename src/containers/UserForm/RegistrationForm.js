import React, { Component } from "react";
import { NavLink } from "react-router-dom";
import { Formik } from "formik";
import * as yup from "yup";
import { Form, Button, Row, Col, Navbar } from "react-bootstrap";
import Logo from "../../components/Navigation/Logo/Logo";
import apiCall from "../../services/apiCall";
import classes from "./UserForm.module.css";
const schema = yup.object({
  name: yup.string().min(3).required("Name is required"),
  email: yup
    .string()
    .email("Email, must be a valid email")
    .required("Email is required"),
  password: yup
    .string()
    .required("Password is required")
    .matches(
      /^.*(?=.{8,})((?=.*[!@#$%^&*()\-_=+{};:,<.>]){1})(?=.*\d)(((?=.*[a-z]){1})((?=.*[A-Z]){1}).*)|((?=.*\d)((?=.*[а-я]){1})((?=.*[А-Я]){1}).*)$/,
      "Must Contain 8 Characters, One Uppercase, One Lowercase, One Number and one special case Character"
    ),
  confirmPassword: yup
    .string()
    .required("Please retype your password")
    .oneOf([yup.ref("password"), null], "Passwords doesn't match"),
});
class RegistrationForm extends Component {
  onSubmitHandler = (values) => {
    apiCall({
      url: "users",
      method: "POST",
      data: {
        name: values.name,
        email: values.email,
        password: values.password,
      },
    }).then((res) => {
      this.props.history.push("/login");
    });
  };
  onCancelHandler = () => {
    this.props.history.push("/login");
  };
  componentDidUpdate = () => {
    const token = localStorage.getItem("AccessToken");
    if (token) {
      this.props.history.push("/");
    }
  };
  componentDidMount = () => {
    const token = localStorage.getItem("AccessToken");
    if (token) {
      this.props.history.push("/");
    }
  };
  render() {
    const initialValues = {
      name: "",
      email: "",
      password: "",
      confirmPassword: "",
    };
    return (
      <>
        <Formik
          validationSchema={schema}
          onSubmit={async (values, { resetForm }) => {
            this.onSubmitHandler(values);
            resetForm();
          }}
          initialValues={initialValues}
        >
          {({
            handleSubmit,
            handleChange,
            resetForm,
            handleBlur,
            values,
            touched,
            isValid,
            errors,
          }) => (
            <div className={classes.Container}>
              <Row className="w-100 mx-auto px-0">
                <Col className="w-100 mx-0 px-0">
                  <Navbar className={classes.FirstRow}>
                    <div>
                      <div>User registration</div>
                      <Navbar.Brand href="/">
                        <Logo> Money Manager K</Logo>
                      </Navbar.Brand>
                    </div>
                  </Navbar>
                </Col>
              </Row>
              <Row className="w-100 mx-auto p-2 pt-4">
                <Col>
                  <Form noValidate onSubmit={handleSubmit} method="POST">
                    <Form.Group>
                      <Form.Label>Name:</Form.Label>
                      <Form.Control
                        type="text"
                        name="name"
                        placeholder="Enter your name"
                        value={values.name || ""}
                        onChange={handleChange}
                        isInvalid={errors.name}
                      />
                      <Form.Control.Feedback type="invalid">
                        {errors.name}
                      </Form.Control.Feedback>
                    </Form.Group>
                    <Form.Group>
                      <Form.Label>Email address:</Form.Label>
                      <Form.Control
                        type="email"
                        name="email"
                        placeholder="Enter email"
                        value={values.email || ""}
                        onChange={handleChange}
                        isInvalid={errors.email}
                      />
                      <Form.Control.Feedback type="invalid">
                        {errors.email}
                      </Form.Control.Feedback>

                      <Form.Text className="text-muted">
                        We'll never share your email with anyone else.
                      </Form.Text>
                    </Form.Group>
                    <Form.Group>
                      <Form.Label>Password:</Form.Label>
                      <Form.Control
                        type="password"
                        name="password"
                        placeholder="Password"
                        value={values.password || ""}
                        onChange={handleChange}
                        isInvalid={errors.password}
                      />
                      <Form.Control.Feedback type="invalid">
                        {errors.password}
                      </Form.Control.Feedback>
                    </Form.Group>
                    <Form.Group>
                      <Form.Label>Retype Password:</Form.Label>
                      <Form.Control
                        type="password"
                        name="confirmPassword"
                        placeholder="Password"
                        onChange={handleChange}
                        value={values.confirmPassword || ""}
                        isInvalid={errors.confirmPassword}
                      />
                      <Form.Control.Feedback type="invalid">
                        {errors.confirmPassword}
                      </Form.Control.Feedback>
                    </Form.Group>
                    <div className={classes.ButtonWrapper}>
                      <Button
                        variant="primary"
                        type="submit"
                        /*                         onClick={this.onSubmitHandler}
                         */
                      >
                        Register
                      </Button>
                      <Button
                        variant="outline-secondary"
                        onClick={this.onCancelHandler}
                        title="Return to Login"
                      >
                        Cancel
                      </Button>
                    </div>
                  </Form>
                </Col>
              </Row>
              <Row className="w-100 mx-auto pb-4">
                <Col>
                  <hr />
                  By registering to our website, you agree with our{" "}
                  <NavLink to="/privacy">Privacy Statement</NavLink> and{" "}
                  <NavLink to="/tos">Terms of service</NavLink>.
                </Col>
              </Row>
            </div>
          )}
        </Formik>
      </>
    );
  }
}

export default RegistrationForm;
