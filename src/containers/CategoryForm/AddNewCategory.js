import React, { Component } from "react";
import { connect } from "react-redux";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faReceipt,
  faHouseDamage,
  faHourglass,
  faHotel,
  faHeadphonesAlt,
  faHandshake,
  faHeart,
  faHammer,
  faTemperatureHigh,
  faPrayingHands,
  faMugHot,
  faCandyCane,
  faRoad,
  faGlasses,
  faAward,
  faPlaneDeparture,
  faSwimmer,
  faMountain,
  faPlaceOfWorship,
  faPeopleCarry,
  faChess,
  faGlassCheers,
  faShoppingCart,
  faShippingFast,
  faLightbulb,
  faCouch,
  faBabyCarriage,
  faDice,
  faChargingStation,
  faMicrochip,
  faSadTear,
  faBirthdayCake,
  faSnowman,
} from "@fortawesome/free-solid-svg-icons";

import { Form, FormCheck, Button, FormControl } from "react-bootstrap";
//TODO IsValid function
const isFormValid = (state) => {
  if (state.name !== "" && state.icon !== "") {
    return true;
  }
  return false;
};
class AddNewCategory extends Component {
  state = { name: "", type: "", icon: "", errors: { name: "", icon: "" } };
  onIconSelect = (event) => {
    const tempStateErrors = this.state.errors;
    const iconValue =
      event.target.id === this.state.icon ? "" : event.target.id;
    tempStateErrors.icon =
      iconValue.length > 0 ? "" : "Icon is required. Please select one";
    this.setState({ icon: iconValue, errors: tempStateErrors });
  };
  onChangeNameHandler = (event) => {
    const tempStateErrors = this.state.errors;
    const nameValue = event.target.value;
    tempStateErrors.name =
      nameValue.trim().length > 0
        ? nameValue.trim().length < 3
          ? "Category name must be at least 3 symbols"
          : ""
        : "Category name is required";
    this.setState({ name: nameValue.trim(), errors: tempStateErrors });
  };
  onSubmitHandler = (event) => {
    event.preventDefault();

    if (isFormValid(this.state)) {
      this.props.onAddCategory(this.state);
    }
  };
  componentDidUpdate = (prevProps, prevState) => {
    prevProps.categoryType !== this.props.categoryType &&
      this.setState({ type: this.props.categoryType });
  };
  componentDidMount = () => {
    this.setState({ type: this.props.categoryType });
  };
  render() {
    const icons = {
      faHouseDamage: faHouseDamage,
      faHourglass: faHourglass,
      faHotel: faHotel,
      faHeadphonesAlt: faHeadphonesAlt,
      faHandshake: faHandshake,
      faHeart: faHeart,
      faHammer: faHammer,
      faTemperatureHigh: faTemperatureHigh,
      faPrayingHands: faPrayingHands,
      faMugHot: faMugHot,
      faCandyCane: faCandyCane,
      faRoad: faRoad,
      faGlasses: faGlasses,
      faAward: faAward,
      faPlaneDeparture: faPlaneDeparture,
      faSwimmer: faSwimmer,
      faMountain: faMountain,
      faPlaceOfWorship: faPlaceOfWorship,
      faPeopleCarry: faPeopleCarry,
      faChess: faChess,
      faGlassCheers: faGlassCheers,
      faShoppingCart: faShoppingCart,
      faShippingFast: faShippingFast,
      faLightbulb: faLightbulb,
      faCouch: faCouch,
      faBabyCarriage: faBabyCarriage,
      faDice: faDice,
      faChargingStation: faChargingStation,
      faMicrochip: faMicrochip,
      faReceipt: faReceipt,
      faSadTear: faSadTear,
      faBirthdayCake: faBirthdayCake,
      faSnowman: faSnowman,
    };
    return (
      <div>
        <legend className={"w-100 pt-1 mt-2 " + this.props.classes.Legend}>
          Add new Category:{" "}
        </legend>

        <Form.Group>
          <Form.Label className={""}>Category Name:</Form.Label>
          <Form.Control
            type="text"
            name="categoryName"
            value={this.state.name}
            onChange={this.onChangeNameHandler}
            isInvalid={this.state.errors.name.length > 0}
          ></Form.Control>
          <FormControl.Feedback type="invalid">
            {this.state.errors.name}
          </FormControl.Feedback>
        </Form.Group>
        <Form.Group bsPrefix="form-group d-flex flex-wrap justify-content-start w-100">
          <Form.Label className="w-100 text-left">
            Select icon:
            <span className="invalid-feedback d-inline ml-3">
              {this.state.errors.icon}
            </span>
          </Form.Label>
          {Object.keys(icons).map((key) => {
            const iconStyle =
              key === this.state.icon
                ? "form-check-label " +
                  this.props.classes.CursorPointer +
                  " px-0 text-center " +
                  this.props.classes.IconSelect +
                  " " +
                  this.props.classes.IconSelected
                : "form-check-label " +
                  this.props.classes.CursorPointer +
                  " px-0 text-center " +
                  this.props.classes.IconSelect;
            return (
              <FormCheck
                key={key}
                bsPrefix={
                  "form-check d-flex flex-wrap justify-content-evenly mx-0 p-1 " +
                  this.props.classes.IconSelectParent
                }
              >
                <FormCheck.Input
                  type={"radio"}
                  name="addCategoryIcon"
                  id={key}
                  onChange={this.onIconSelect}
                  onClick={this.onIconSelect}
                  bsPrefix="d-none mx-0 px-0"
                />
                <FormCheck.Label htmlFor={key} bsPrefix={iconStyle}>
                  <FontAwesomeIcon icon={icons[key]} />
                </FormCheck.Label>
              </FormCheck>
            );
          })}
        </Form.Group>
        <div className="w-100 text-right">
          <Button
            variant="outline-success"
            className="m-auto"
            if="addButton"
            onClick={this.onSubmitHandler}
            disabled={!isFormValid(this.state)}
          >
            Add
          </Button>
        </div>
      </div>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    categories: state.categoryState.get("category"),
  };
};

export default connect(mapStateToProps, null)(AddNewCategory);
