import React from "react";
import moment from "moment";
import { connect } from "react-redux";
import { selectDateChange } from "../../redux/actions/appActions";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faLessThan, faGreaterThan } from "@fortawesome/free-solid-svg-icons";

import classes from "./DateSelector.module.css";

const DateSelector = (props) => {
  const dateChangeHandler = (date, type) => {
    const newPath =
      props.location.pathname.slice(0, props.location.pathname.length - 10) +
      moment(date).format("YYYY-MM-DD");
    props.selectDateChange(date, type);
    props.history.push(newPath);
  };

  const selectDateRenderer =
    props.dateType === "month"
      ? moment(props.selectDate).format("MMM, YYYY")
      : moment(props.selectDate).format("YYYY");
  const incDate = new Date(
    moment(props.selectDate).add(1, props.dateType).format("DD MMMM,YYYY")
  );
  const decDate = new Date(
    moment(props.selectDate).subtract(1, props.dateType).format("DD MMMM,YYYY")
  );

  return (
    <div className={classes.SelectDate}>
      <button
        className={classes.Arrow}
        onClick={() => dateChangeHandler(decDate, props.dateType)}
      >
        <FontAwesomeIcon icon={faLessThan} />
      </button>
      <div>{selectDateRenderer}</div>
      <button
        className={classes.Arrow}
        onClick={() => dateChangeHandler(incDate, props.dateType)}
      >
        <FontAwesomeIcon icon={faGreaterThan} />
      </button>
    </div>
  );
};

const mapStateToProps = (state) => {
  return {
    selectDate: state.appState.get("selectDate"),
    dateType: state.appState.get("dateType"),
  };
};
const mapDispatchToProps = {
  selectDateChange: selectDateChange,
};

export default connect(mapStateToProps, mapDispatchToProps)(DateSelector);
